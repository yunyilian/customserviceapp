package com.yunyilian8.customserviceapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.AppInfo;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseActivity;
import com.yunyilian8.customserviceapp.bean.ResultBean;
import com.yunyilian8.customserviceapp.service.RetrofitService;
import com.yunyilian8.customserviceapp.utils.JSONUtils;
import com.yunyilian8.customserviceapp.utils.MatchUtil;
import com.yunyilian8.customserviceapp.utils.ToastUtil;
import com.yunyilian8.customserviceapp.view.TitleBarLayout;

import rx.Subscriber;

/**
 * 服务定制详情页面
 */

public class CustomDetailsActivity extends BaseActivity implements View.OnClickListener {
    private String title_name;
    private Integer typeInt;
    private TitleBarLayout titlebar_layout;
    private TextView tv_add;
    private EditText name;
    private EditText phone;
    private EditText requirements;
    /**
     * 默认手机号输入不合法
     **/
    private boolean INPUT_PHONE = false;
    /**
     * 默认姓名没有输入
     **/
    private boolean INPUT_NAME = false;
    /**
     * 默认需求没有输入
     **/
    private boolean INPUT_REQUIREMENTS = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_details);

        initView();
        setEditTextListener();
        initEvent();
    }

    private void initView() {
        titlebar_layout = (TitleBarLayout) findViewById(R.id.titlebar_layout);
        tv_add = (TextView) findViewById(R.id.tv_add);
        name = (EditText) findViewById(R.id.et_input_name);
        phone = (EditText) findViewById(R.id.et_input_phone);
        requirements = (EditText) findViewById(R.id.et_requirements);

        Intent intent = getIntent();
        title_name = intent.getStringExtra("typeString");
        typeInt = intent.getIntExtra("typeInt", -1);
        switch (typeInt) {
            case 0:
                titlebar_layout.setTitleText("IOS APP");
                break;
            case 1:
                titlebar_layout.setTitleText("安卓APP");
                break;
            case 2:
                titlebar_layout.setTitleText("微信公众号");
                break;
            case 3:
                titlebar_layout.setTitleText("Web 网站");
                break;
            case 4:
                titlebar_layout.setTitleText("手机网站");
                break;
            case 5:
                titlebar_layout.setTitleText("智能硬件APP");
                break;
            case 6:
                titlebar_layout.setTitleText("桌面软件");
                break;
            case 7:
                titlebar_layout.setTitleText("VR APP");
                break;
            default:
                break;
        }
    }


    /**
     * 设置EditText的监听
     */
    private void setEditTextListener() {
        /**电话EditText的监听**/
        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (MatchUtil.isMobileNO(charSequence.toString().trim())) {
                    INPUT_PHONE = true;
                } else {
                  /*  *手机号码输入不合法**/
                    INPUT_PHONE = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    INPUT_NAME = true;
                } else {
                    INPUT_NAME = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        requirements.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    INPUT_REQUIREMENTS = true;
                } else {
                    INPUT_REQUIREMENTS = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private void initEvent() {
        tv_add.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        toSumbit(true);
    }


    /**
     * 去注册
     */
    private void toSumbit(boolean showDialog) {
        String phones = phone.getText().toString().trim();
        String names = name.getText().toString().trim();
        String requirement = requirements.getText().toString().trim();

        if (!INPUT_NAME || !INPUT_REQUIREMENTS ) {
            ToastUtil.showToast(mAppContext, "请填写完整信息!");
            return;
        }
        if( !INPUT_PHONE ){
            ToastUtil.showToast(mAppContext, "电话输入有误!");
            return;
        }
        loadDataFromNet(new Subscriber<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

                ToastUtil.showToast(mAppContext,"获取数据失败！");
            }

            @Override
            public void onNext(String result) {
                hideLoadingView();
                try {
                    if (result != null) {
                        processData(result);
                    } else {
                        ToastUtil.showToast(mAppContext, "获取数据失败");
                    }
                } catch (Exception e) {
                }
            }
        }, RetrofitService.getInstance().submitRequirements(names,phones,requirement,typeInt.toString(), AppInfo.mInfo.getId()));
        if (showDialog)
            showLoadingView();
    }

    /**
     * 处理数据
     * @param result
     */
    private void processData(String result) {
        String status = JSONUtils.getValue(result, "status");
        if (ResultBean.SUCCESSfUL.equals(status))
        {
            /**提交成功**/
            ToastUtil.showToast(mAppContext, "需求提交成功！");
            finish();
        }else if (ResultBean.FAILED.equals(status)){
            /**提交失败**/
            ToastUtil.showToast(mAppContext, "需求提交失败！");

            /**清空输入**/
            name.setText("");
            phone.setText("");
            requirements.setText("");
            name.requestFocus();//获取焦点
        }
    }
}
