package com.yunyilian8.customserviceapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.AppInfo;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.adapter.HomeViewPagerAdapter;
import com.yunyilian8.customserviceapp.base.BaseFragmentActivity;
import com.yunyilian8.customserviceapp.bean.DevelopmentSchemeRecyclerBean;
import com.yunyilian8.customserviceapp.fragment.DeveloperDetailIndemnifyFragment;
import com.yunyilian8.customserviceapp.fragment.DeveloperDetailServiceFragment;
import com.yunyilian8.customserviceapp.utils.AnyEventType;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2016/12/14.
 * 开发者-详细信息页面
 */

public class DeveloperDetailActivity extends BaseFragmentActivity {
    @InjectView(R.id.tv_name)
    TextView mTvName;
    @InjectView(R.id.tv_service_content)
    TextView mTvServiceContent;
    @InjectView(R.id.tv_address)
    TextView mTvAddress;
    @InjectView(R.id.tv_info)
    TextView mTvInfo;
    @InjectView(R.id.tv_left)
    TextView mTvLeft;
    @InjectView(R.id.tv_right)
    TextView mTvRight;
    @InjectView(R.id.cvp_viewPager)
    ViewPager mViewPager;

    /**
     * fragment管理器
     **/
    private FragmentManager mFragmentManager;
    /**
     * 用于填充viewPager的list数据
     **/
    private List<Fragment> mFragmentList = new ArrayList<>();
    private DevelopmentSchemeRecyclerBean mBean;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppInfo.addActivity(this);
        setContentView(R.layout.activity_developer_detail);
        ButterKnife.inject(this);
        EventBus.getDefault().register(this);

        getIntentData();
        /**初始化数据**/
        initData();
        /**初始化fragments**/
        initFragments();

    }


    /**
     * 接受事件广播
     */
    public void onEventMainThread(AnyEventType event) {
        switch (event.getType()) {

        }
    }

    /**
     * 初始化数据
     */
    private void initData() {
        //TODO 初始化数据
    }

    /**
     * 初始化fragments
     */
    private void initFragments() {
        try {
            mFragmentManager = this.getSupportFragmentManager();

            DeveloperDetailServiceFragment fragment1 = new DeveloperDetailServiceFragment();
            DeveloperDetailIndemnifyFragment fragment2 = new DeveloperDetailIndemnifyFragment();

            /**给fragment传递数据**/
            Bundle bundle = new Bundle();
            bundle.putParcelable("bean", mBean);
            fragment1.setArguments(bundle);
            fragment2.setArguments(bundle);

            /**'提供的服务'页面**/
            mFragmentList.add(fragment1);

            /**服务保障'界面**/
            mFragmentList.add(fragment2);

            mViewPager.setAdapter(new HomeViewPagerAdapter(mFragmentList, mFragmentManager));
            mViewPager.setCurrentItem(0);
            mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    switch (position) {
                        case 0:
                            mTvLeft.setTextColor(getResources().getColor(R.color.red));
                            mTvRight.setTextColor(getResources().getColor(R.color.black));
                            break;
                        case 1:
                            mTvRight.setTextColor(getResources().getColor(R.color.red));
                            mTvLeft.setTextColor(getResources().getColor(R.color.black));
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.tv_left, R.id.tv_right})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_left:
                /**提供的服务**/
                mViewPager.setCurrentItem(0);
                mTvLeft.setTextColor(getResources().getColor(R.color.red));
                mTvRight.setTextColor(getResources().getColor(R.color.black));

                break;
            case R.id.tv_right:
                /**服务保障**/
                mViewPager.setCurrentItem(1);
                mTvRight.setTextColor(getResources().getColor(R.color.red));
                mTvLeft.setTextColor(getResources().getColor(R.color.black));

                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void getIntentData() {
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");
        mBean = bundle.getParcelable("bean");

    }
}
