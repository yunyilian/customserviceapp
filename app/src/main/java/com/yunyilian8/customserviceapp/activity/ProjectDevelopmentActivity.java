package com.yunyilian8.customserviceapp.activity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseActivity;
import com.yunyilian8.customserviceapp.view.TitleBarLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * 项目开发页面
 */

public class ProjectDevelopmentActivity extends BaseActivity {

    @InjectView(R.id.mian_name)
    TextView mMianName;
    @InjectView(R.id.tv_content)
    TextView mTvContent;
    @InjectView(R.id.spread)
    ImageView mSpread;
    @InjectView(R.id.shrink_up)
    ImageView mShrinkUp;
    @InjectView(R.id.show_more)
    RelativeLayout mShowMore;
    @InjectView(R.id.tv_strength_description)
    TextView mTv;

    private static final int VIDEO_CONTENT_DESC_MAX_LINE = 3;// 默认展示最大行数3行
    private static final int SHOW_CONTENT_NONE_STATE = 0;// 扩充
    private static final int SHRINK_UP_STATE = 1;// 收起状态
    private static final int SPREAD_STATE = 2;// 展开状态
    private static int mState = SHRINK_UP_STATE;//默认收起状态
    @InjectView(R.id.titlebar_layout)
    TitleBarLayout mTitlebarLayout;
    @InjectView(R.id.tv_01)
    TextView mTextView1;
    @InjectView(R.id.tv_02)
    TextView mTextView2;
    @InjectView(R.id.tv_03)
    TextView mTextView3;
    @InjectView(R.id.tv_06)
    TextView mTextView6;
    @InjectView(R.id.tv_05)
    TextView mTextView5;
    @InjectView(R.id.tv_04)
    TextView mTextView4;
    @InjectView(R.id.tv_07)
    TextView mTextView7;
    @InjectView(R.id.tv_08)
    TextView mTextView8;
    @InjectView(R.id.tv_09)
    TextView mTextView9;
    @InjectView(R.id.tv_11)
    TextView mTextView11;
    @InjectView(R.id.tv_10)
    TextView mTextView10;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        ButterKnife.inject(this);

        initData();

        setWorkProgress();
    }

    /**
     * 设置工作进度
     */
    private void setWorkProgress() {
        /**设置模拟数据**/
        int progress = 4;

        for (int i = 1; i <= progress; i++) {
            if (i == 1) {
                mTextView1.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 2) {
                mTextView2.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 3) {
                mTextView3.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 4) {
                mTextView4.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 5) {
                mTextView5.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 6) {
                mTextView6.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 7) {
                mTextView7.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 8) {
                mTextView8.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 9) {
                mTextView9.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 10) {
                mTextView10.setBackgroundResource(R.drawable.shape_transparency_red);
            } else if (i == 11) {
                mTextView11.setBackgroundResource(R.drawable.shape_transparency_red);
            }
        }
    }

    private void initData() {
        String data01 = "开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力";
        String data02 = "开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团队的实力开团";
        //TODO huoqu


        mTvContent.setText(data01);
        String title = "开团队的实力";
        String str = "<font color='#FF0000'>" + title + "：</font>" + data02;
        mTv.setTextSize(18);
        mTv.setText(Html.fromHtml(str));
    }

    @OnClick({R.id.show_more})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_more:
                if (mState == SPREAD_STATE) {
                    mTvContent.setMaxLines(VIDEO_CONTENT_DESC_MAX_LINE);
                    mTvContent.requestLayout();
                    mShrinkUp.setVisibility(View.GONE);
                    mSpread.setVisibility(View.VISIBLE);
                    mState = SHRINK_UP_STATE;
                } else if (mState == SHRINK_UP_STATE) {
                    mTvContent.setMaxLines(Integer.MAX_VALUE);
                    mTvContent.requestLayout();
                    mShrinkUp.setVisibility(View.VISIBLE);
                    mSpread.setVisibility(View.GONE);
                    mState = SPREAD_STATE;
                }
        }
    }
}
