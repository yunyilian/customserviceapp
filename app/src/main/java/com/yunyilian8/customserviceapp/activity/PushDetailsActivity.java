package com.yunyilian8.customserviceapp.activity;

import android.os.Bundle;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseActivity;

/**
 * 详情页面（推过来的任务）
 */

public class PushDetailsActivity extends BaseActivity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_details);
    }
}
