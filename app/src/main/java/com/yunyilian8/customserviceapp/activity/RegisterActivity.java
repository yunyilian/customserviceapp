package com.yunyilian8.customserviceapp.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseActivity;
import com.yunyilian8.customserviceapp.bean.ResultBean;
import com.yunyilian8.customserviceapp.service.RetrofitService;
import com.yunyilian8.customserviceapp.utils.JSONUtils;
import com.yunyilian8.customserviceapp.utils.KeyboardUtil;
import com.yunyilian8.customserviceapp.utils.MD5Utils;
import com.yunyilian8.customserviceapp.utils.ToastUtil;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import rx.Subscriber;

/**
 * 注册页面
 */

public class RegisterActivity extends BaseActivity {
    @InjectView(R.id.et_phone)
    EditText mEtPhone;
    @InjectView(R.id.et_password)
    EditText mEtPassword;
    @InjectView(R.id.et_password2)
    EditText mEtPassword2;
    @InjectView(R.id.tv_reg)
    TextView mTvReg;

    /**
     * 默认手机号输入不合法
     **/
    private boolean INPUT_PHONE = false;
    /**
     * 默认密码01没有输入
     **/
    private boolean INPUT_PASSWORD01 = false;
    /**
     * 默认密码02没有输入
     **/
    private boolean INPUT_PASSWORD02 = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.inject(this);

        /**设置EditText的监听**/
        setEditTextListener();
    }


    /**
     * 设置EditText的监听
     */
    private void setEditTextListener() {
        /**电话EditText的监听**/
     /*   mEtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (MatchUtil.isMobileNO(charSequence.toString().trim())) {

                    INPUT_PHONE = true;
                } else {
                    *//**手机号码输入不合法**//*
                    INPUT_PHONE = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/

        /**密码EditText的监听**/
        mEtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    INPUT_PASSWORD01 = true;
                } else {
                    INPUT_PASSWORD01 = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        /**密码2EditText的监听**/
        mEtPassword2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    INPUT_PASSWORD02 = true;
                } else {
                    INPUT_PASSWORD02 = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @OnClick(R.id.tv_reg)
    public void onClick() {
        /**注册**/
        toReg(true);
    }

    /**
     * 去注册
     */
    private void toReg(boolean showDialog) {
        String phoneNum = mEtPhone.getText().toString().trim();
        String password01 = mEtPassword.getText().toString().trim();
        String password02 = mEtPassword2.getText().toString().trim();


        if (!INPUT_PASSWORD01 || !INPUT_PASSWORD02 ) {
            ToastUtil.showToast(mAppContext, "请输入密码!");
            return;
        }else if (!password01.equals(password02))
        {
            ToastUtil.showToast(mAppContext, "密码输入不一致！");
            return;
        }
        password01 = MD5Utils.GetMD5Code(password01 + RetrofitService.passwordMD5);

        loadDataFromNet(new Subscriber<String>() {
            @Override
            public void onCompleted() {
            }
            @Override
            public void onError(Throwable throwable) {
                ToastUtil.showToast(mAppContext,"获取数据失败！");
            }
            @Override
            public void onNext(String result) {
                hideLoadingView();
                try {
                    if (result != null) {
                        processData(result);
                    } else {
                        ToastUtil.showToast(mAppContext, "获取数据失败");
                    }
                } catch (Exception e) {
                }
            }
        }, RetrofitService.getInstance().userRegister(phoneNum,password01));
        if (showDialog)
            showLoadingView();
    }

    /**
     * 处理数据
     * @param result
     */
    private void processData(String result) {
        String status = JSONUtils.getValue(result, "status");
        if (ResultBean.SUCCESSfUL.equals(status))
        {
            /**请求成功**/
            ToastUtil.showToast(mAppContext, "注册成功！");
            /**隐藏键盘**/
            KeyboardUtil.hideInputMethodWindow(this);
            finish();
        }else if (ResultBean.FAILED.equals(status)){
            /**注册失败**/
            ToastUtil.showToast(mAppContext, "注册失败！");

            /**清空输入**/
            mEtPhone.setText("");
            mEtPassword2.setText("");
            mEtPassword.setText("");
            mEtPhone.requestFocus();//获取焦点
        }
    }
}
