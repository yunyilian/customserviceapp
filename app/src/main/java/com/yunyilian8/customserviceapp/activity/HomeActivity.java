package com.yunyilian8.customserviceapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.yunyilian8.customserviceapp.AppInfo;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.adapter.HomeViewPagerAdapter;
import com.yunyilian8.customserviceapp.base.BaseFragmentActivity;
import com.yunyilian8.customserviceapp.fragment.HomeCustomFragment;
import com.yunyilian8.customserviceapp.fragment.HomeMyFragment;
import com.yunyilian8.customserviceapp.utils.StatusBarUtil;
import com.yunyilian8.customserviceapp.utils.ToastUtil;
import com.yunyilian8.customserviceapp.utils.UITools;
import com.yunyilian8.customserviceapp.utils.VersionUtils;
import com.yunyilian8.customserviceapp.view.CustomViewPager;
import com.yunyilian8.customserviceapp.view.FixedSpeedScroller;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * 首页activity
 */
public class HomeActivity extends BaseFragmentActivity implements CompoundButton.OnCheckedChangeListener {

    @InjectView(R.id.rbtn_home_home)
    RadioButton mRbtn01;
    @InjectView(R.id.rbtn_home_challenge)
    RadioButton mRbtn02;
    @InjectView(R.id.cvp_viewPager)
    CustomViewPager mViewPager;
    @InjectView(R.id.rl)
    RelativeLayout mLlLayout;

    /**
     * fragment管理器
     **/
    private FragmentManager mFragmentManager;
    /**
     * 用于填充viewPager的list数据
     **/
    private List<Fragment> mFragmentList = new ArrayList<>();
    private Context mAppContext;
    private boolean mIsMenuOpened = false;
    private long mExitTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppInfo.addActivity(this);
        setContentView(R.layout.activity_main);
        mAppContext = getApplicationContext();
        ButterKnife.inject(this);

        /**设置button的监听**/
        setButtonListener();
        /**初始化fragments**/
        initFragments();
        ToastUtil.showToast(mAppContext, (null != AppInfo.mInfo)?AppInfo.mInfo.getUsername():"没有登录");
    }

    /**
     * 设置button的监听
     */
    private void setButtonListener() {
        mRbtn01.setOnCheckedChangeListener(this);
        mRbtn02.setOnCheckedChangeListener(this);
    }

    /**
     * 初始化fragments
     */
    private void initFragments() {
        try {
            mFragmentManager = this.getSupportFragmentManager();

            /**'外包'页面**/
            mFragmentList.add(new HomeCustomFragment());

            /**我的'界面**/
            mFragmentList.add(new HomeMyFragment());

            /**设置ViewPager是否可以滑动**/
            mViewPager.setSlipping(false);
            // mViewPager.setOnPageChangeListener(this);
            mViewPager.setAdapter(new HomeViewPagerAdapter(mFragmentList, mFragmentManager));
            Field mField = ViewPager.class.getDeclaredField("mScroller");
            mField.setAccessible(true);
            mField.set(mViewPager, new FixedSpeedScroller(mViewPager.getContext(), new AccelerateInterpolator()));
            mViewPager.setCurrentItem(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick({R.id.rbtn_home_home, R.id.rbtn_home_challenge, R.id.tv_add})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rbtn_home_home:
                /**软件外包**/
                mViewPager.setCurrentItem(0);
                mRbtn02.setChecked(false);
                /**动态设置状态栏颜色为黄色**/
                if (VersionUtils.aboveVersion21()) {
                    StatusBarUtil.setStatusBarColor(this, R.color.colorPrimaryDark);
                    mLlLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                    mViewPager.setTop(UITools.dip2px(mCtx, 20));
                }

                break;
            case R.id.rbtn_home_challenge:
                /**我的**/
                mViewPager.setCurrentItem(1);
                mRbtn01.setChecked(false);
                /**动态设置状态栏为全透明，并设置全屏**/
                if (VersionUtils.aboveVersion21()) {
                    mLlLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                    StatusBarUtil.setStatusBarColor(this, R.color.transparent);
                    //         StatusBarUtil.transparencyBar(this);
                }

                mViewPager.setTop(0);

                break;
            case R.id.tv_add:
                /**添加**/
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            buttonView.setTextColor(getResources().getColor(R.color.red));
        } else {
            buttonView.setTextColor(getResources().getColor(R.color.black));
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!mIsMenuOpened) {
                if ((System.currentTimeMillis() - mExitTime) > 2000) {
                    ToastUtil.showToast(mAppContext, "再按一次退出程序");
                    mExitTime = System.currentTimeMillis();
                } else {
                    finish();
                }
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
