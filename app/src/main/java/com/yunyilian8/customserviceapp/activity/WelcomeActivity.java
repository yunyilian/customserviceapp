package com.yunyilian8.customserviceapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.yunyilian8.customserviceapp.AppInfo;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseActivity;
import com.yunyilian8.customserviceapp.bean.UserInfoBean;
import com.yunyilian8.customserviceapp.utils.Contants;
import com.yunyilian8.customserviceapp.utils.SerializeManager;
import com.yunyilian8.customserviceapp.utils.ToastUtil;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2016/12/27.
 * 欢迎页
 */

public class WelcomeActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        setLogin();
    }

    /**
     * 设置是不是进入登录页面
     */
    private void setLogin() {
        mCompositeSubscription.add(Observable
                .timer(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.immediate())
                .map(new Func1<Long, Boolean>() {
                    @Override
                    public Boolean call(Long aLong) {
                        /**/
                        AppInfo.mInfo = (UserInfoBean) SerializeManager.loadFile(Contants.UserInfo);
                        ToastUtil.showToast(mAppContext,(null == AppInfo.mInfo)?"空":"info");
                        return TextUtils.isEmpty((null == AppInfo.mInfo)?null:"info");
                    }
                })
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        if (aBoolean) {
                            /**进入用户登录界面**/
                            startActivity(new Intent(mCtx, LoginActivity.class));
                            finishToNewActivity();
                        } else {
                            /**进入到首页界面**/
                            startActivity(new Intent(mCtx, HomeActivity.class));
                            finishToNewActivity();
                        }
                    }
                }));
    }
}
