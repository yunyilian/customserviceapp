package com.yunyilian8.customserviceapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.adapter.DevelopmentSchemeRecyclerAdapter;
import com.yunyilian8.customserviceapp.adapter.MyRecyclerviewAdapter;
import com.yunyilian8.customserviceapp.adapter.SpinerAdapter;
import com.yunyilian8.customserviceapp.base.BaseActivity;
import com.yunyilian8.customserviceapp.bean.DevelopmentSchemeRecyclerBean;
import com.yunyilian8.customserviceapp.utils.AnimationUtils;
import com.yunyilian8.customserviceapp.utils.ToastUtil;
import com.yunyilian8.customserviceapp.utils.UITools;
import com.yunyilian8.customserviceapp.view.DividerGridItemDecoration;
import com.yunyilian8.customserviceapp.view.MyScrollView02;
import com.yunyilian8.customserviceapp.view.SpinerPopWindow;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Administrator on 2016/12/15.
 * 开发方式界面
 */

public class DevelopmentSchemeActivity extends BaseActivity implements SpinerAdapter.IOnItemSelectListener {
    private static final int HORIZONTAL_NUM = 3;
    /**
     * 设置Recyclerview列表item的左右距离
     **/
    private static final int LEFT_RIGHT_MAGIN = 25;
    /**
     * 设置Recyclerview列表item的上下距离
     **/
    private static final int TOP_BOTTOM_MAGIN = 10;
    @InjectView(R.id.tv_title01)
    TextView mTvTitle01;
    @InjectView(R.id.tv_title02)
    TextView mTvTitle02;
    @InjectView(R.id.tv_title03)
    TextView mTvTitle03;
    @InjectView(R.id.tv_welcome)
    TextView mTvWelcome;
    @InjectView(R.id.tv_welcome02)
    TextView mTvWelcome02;
    @InjectView(R.id.tv_new)
    TextView mTvNew;
    @InjectView(R.id.tv_new02)
    TextView mTvNew02;
    @InjectView(R.id.tv_spinner)
    TextView mTvSpinner;
    @InjectView(R.id.tv_spinner02)
    TextView mTvSpinner02;
    @InjectView(R.id.ll_spinner)
    LinearLayout mLlSpinner;
    @InjectView(R.id.rv_grid)
    RecyclerView mRecyclerView;
    @InjectView(R.id.msv_scrollView)
    MyScrollView02 mScrollView;
    @InjectView(R.id.ll_title)
    LinearLayout mLlTitle;
    @InjectView(R.id.rl_most)
    RelativeLayout mRlMost;
    @InjectView(R.id.ll_spinner02)
    LinearLayout mLlSpinner02;

    /**
     * 服务的类型，1：带队 开发 2：团队合作 3：上门服务，默认为1
     **/
    private int SERVICE_TYPE = 1;
    /**
     * 之最 1：最受欢迎 2：最新注入  默认1
     **/
    private int SERVICE_MOST = 1;
    /**
     * 默认spinner为第一个
     **/
    private int SERVICE_SPINNER = 0;
    /**
     * 是不是显示的顶部spinner布局
     **/
    private boolean IS_TOP = false;

    private SpinerAdapter mAdapter;
    private List<String> mListType;
    private List<DevelopmentSchemeRecyclerBean> mImageDatas;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_development_cheme);
        ButterKnife.inject(this);

        initRecyclerViewData();
        initRecyclerView();

        initSpinnerData();
        setScrollViewListener();
    }

    /**
     * 初始化RecyclerView
     */
    private void initRecyclerView() {

        /**解决RecyclerView的滑动问题（不能流畅滑动的问题）**/
        UITools.setRecyclerViewScroll(mCtx, mRecyclerView);

        DevelopmentSchemeRecyclerAdapter adapter = new DevelopmentSchemeRecyclerAdapter(mImageDatas, mCtx);
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.setLayoutManager(new GridLayoutManager(mCtx, HORIZONTAL_NUM));
        /**设置分割线**/
        mRecyclerView.addItemDecoration(new DividerGridItemDecoration(mCtx, HORIZONTAL_NUM, LEFT_RIGHT_MAGIN, TOP_BOTTOM_MAGIN));

        //设置监听事件
        adapter.setOnItemClickListener(new MyRecyclerviewAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position, View view) {
                //       gotoVideoList(position, view);
                DevelopmentSchemeRecyclerBean bean = mImageDatas.get(position);
                Intent intent = new Intent(DevelopmentSchemeActivity.this, DeveloperDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("bean", bean);
                intent.putExtra("bundle", bundle);
                startActivity(intent);
            }

            @Override
            public void OnItemLongClick(int position, View view) {

            }
        });
    }

    /**
     * 初始化RecyclerView的数据
     */
    private void initRecyclerViewData() {
        mImageDatas = new ArrayList();
        for (int i = 0; i < 50; i++) {
            DevelopmentSchemeRecyclerBean bean = new DevelopmentSchemeRecyclerBean();
            bean.setURL("http://pic42.nipic.com/20140619/11955670_175345581000_2.jpg");
            bean.setName("王彦斌");
            bean.setKnow("10年工作经验");
            bean.setPrice("1000/日");

            mImageDatas.add(bean);
        }

    }

    /**
     * 设置ScrollView设置监听
     */
    private void setScrollViewListener() {
        final int layoutHeight = AnimationUtils.getMeasuredView(mLlTitle).getMeasuredHeight() + UITools.dip2px(mAppContext, 20);
        mScrollView.setLoadDateListener(new MyScrollView02.LoadDateListener() {
            @Override
            public void isBottom() {

            }

            @Override
            public void notBottom() {

            }

            @Override
            public void scroll(int roll) {
                Log.d("mScrollView", roll + "+" + layoutHeight);
                if (roll > layoutHeight) {
                    IS_TOP = true;
                    mRlMost.setVisibility(View.VISIBLE);
                    mLlSpinner02.setVisibility(View.VISIBLE);
                } else {
                    IS_TOP = false;
                    mRlMost.setVisibility(View.GONE);
                    mLlSpinner02.setVisibility(View.GONE);
                }
            }

            @Override
            public void isTouchStop() {

            }
        });
    }

    private void initSpinnerData() {
        initData();
        mAdapter = new SpinerAdapter(this, mListType);
        mAdapter.refreshData(mListType, 0);

        //显示第一条数据
        mTvSpinner.setText(mListType.get(0));
    }

    @OnClick({R.id.tv_title01, R.id.tv_title02, R.id.tv_title03, R.id.tv_welcome, R.id.tv_new,
            R.id.ll_spinner, R.id.ll_spinner02, R.id.tv_welcome02, R.id.tv_new02})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_title01:
                /**带队开发**/
                setDevelopmentType(1);
                break;
            case R.id.tv_title02:
                /**团队合作**/
                setDevelopmentType(2);
                break;
            case R.id.tv_title03:
                /**上门服务**/
                setDevelopmentType(3);
                break;
            case R.id.tv_welcome:
                /**最受欢迎**/
                setMost(1);
                break;
            case R.id.tv_new:
                /**最新入驻**/
                setMost(2);
                break;
            case R.id.tv_welcome02:
                /**最受欢迎02**/
                setMost(1);
                break;
            case R.id.tv_new02:
                /**最新入驻02**/
                setMost(2);
                break;
            case R.id.ll_spinner:
                /**下拉spinner**/
                showSpinner();
                break;
            case R.id.ll_spinner02:
                /**下拉spinner**/
                showSpinner();
                break;
        }
    }

    private void setDevelopmentType(int type) {
        switch (type) {
            case 1:
                SERVICE_TYPE = 1;

                mTvTitle01.setTextColor(getResources().getColor(R.color.red));
                mTvTitle02.setTextColor(getResources().getColor(R.color.black));
                mTvTitle03.setTextColor(getResources().getColor(R.color.black));
                loadData();
                break;
            case 2:
                SERVICE_TYPE = 2;

                mTvTitle01.setTextColor(getResources().getColor(R.color.black));
                mTvTitle02.setTextColor(getResources().getColor(R.color.red));
                mTvTitle03.setTextColor(getResources().getColor(R.color.black));
                loadData();
                break;
            case 3:
                SERVICE_TYPE = 3;

                mTvTitle01.setTextColor(getResources().getColor(R.color.black));
                mTvTitle02.setTextColor(getResources().getColor(R.color.black));
                mTvTitle03.setTextColor(getResources().getColor(R.color.red));
                loadData();
                break;
        }
    }

    private void setMost(int type) {
        switch (type) {
            case 1:
                SERVICE_MOST = 1;
                mTvWelcome.setTextColor(getResources().getColor(R.color.red));
                mTvWelcome02.setTextColor(getResources().getColor(R.color.red));
                mTvNew.setTextColor(getResources().getColor(R.color.black));
                mTvNew02.setTextColor(getResources().getColor(R.color.black));

                loadData();
                break;
            case 2:
                SERVICE_MOST = 2;
                mTvWelcome.setTextColor(getResources().getColor(R.color.black));
                mTvWelcome02.setTextColor(getResources().getColor(R.color.black));
                mTvNew.setTextColor(getResources().getColor(R.color.red));
                mTvNew02.setTextColor(getResources().getColor(R.color.red));

                loadData();
                break;
        }

    }


    private void showSpinner() {
        SpinerPopWindow mSpinerPopWindow = new SpinerPopWindow(this);
        mSpinerPopWindow.setAdatper(mAdapter);
        mSpinerPopWindow.setItemListener(this);

        if (IS_TOP) {
            mSpinerPopWindow.setWidth(mLlSpinner02.getWidth());
            mSpinerPopWindow.showAsDropDown(mTvSpinner02);
        } else {
            mSpinerPopWindow.setWidth(mLlSpinner.getWidth());
            mSpinerPopWindow.showAsDropDown(mTvSpinner);
        }
    }

    private void initData() {
        //类型列表
        mListType = new ArrayList<String>();
        mListType.add("UI设计");
        mListType.add("IOS开发");
        mListType.add("安卓开发");
        mListType.add("AR开发");
        mListType.add("桌面开发");
    }

    @Override
    public void onItemClick(int pos) {
        if (pos >= 0 && pos <= mListType.size()) {
            String value = mListType.get(pos);

            mTvSpinner02.setText(value.toString());
            mTvSpinner.setText(value.toString());

            SERVICE_SPINNER = pos;

            /**加载数据**/
            loadData();
        }
    }

    private void loadData() {
        //TODO 加载新数据。
        ToastUtil.showToast(mAppContext, "服务的类型:" + SERVICE_TYPE
                + "服务之最：" + SERVICE_MOST + "spinner:" + mListType.get(SERVICE_SPINNER));
    }
}
