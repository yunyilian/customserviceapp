package com.yunyilian8.customserviceapp.activity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseActivity;
import com.yunyilian8.customserviceapp.utils.UITools;
import com.yunyilian8.customserviceapp.view.CustomLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2016/12/16.
 * 验收节点 界面
 */

public class AcceptanceCheckPointActivity extends BaseActivity {
    @InjectView(R.id.cLayout)
    CustomLayout mLlLine;
    private int max = 10;
    private String[] data = new String[]{
            "签订合同","产品原型","UI设计","程序开发","分段验收","Bate版本",
            "线上部署","项目验收","免费维护期","迭代开发"
    };
    private int PROGRESS ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceptance_check_point);
        ButterKnife.inject(this);

        getProgress();
        setData();
    }

    private void setData() {
        /**获取屏幕高度，-状态栏 -头部 - 设置的dp值**/
        int layoutHeight = UITools.getScreenCenterPoint(mCtx).y*2 - UITools.dip2px(mCtx,45)
                - UITools.dip2px(mCtx,20) -  UITools.dip2px(mCtx,20);
        int layoutWidth = UITools.getScreenCenterPoint(mCtx).x*2 - UITools.dip2px(mCtx,mLlLine.getLeft()
                - UITools.dip2px(mCtx,mLlLine.getRight()));

        /**设置中间的图片的高宽为20dp**/
        int viewWidth = UITools.dip2px(mCtx,20);
        int viewHeight = UITools.dip2px(mCtx,20);
        /**获取每个图片之间的距离**/
        int top = (layoutHeight - (max * viewHeight))/(max-1);

        /**循环添加数据到布局中**/
        for (int i = 0; i < max; i ++)
        {
            /**图片的坐标值**/
            int mLeft = layoutWidth/2 - viewWidth /2;
            int mTop = i * top + (i) * viewHeight + UITools.dip2px(mCtx,10);
            int mRight = layoutWidth/2 + viewWidth /2;
            int mBottom = i * top + viewHeight + (i) * viewHeight + UITools.dip2px(mCtx,10);

            /**textView的坐标值**/
            int mTextLeft;
            int mTextTop;
            int mTextRight;
            int mTextBottom;

            if (i % 2 == 0)
            {
                /**右侧文字**/
                mTextLeft = layoutWidth/2 - viewWidth /2 + UITools.dip2px(mCtx,30);
                mTextTop = i * top + (i) * viewHeight + UITools.dip2px(mCtx,10);
                mTextRight = layoutWidth/2 + viewWidth /2 + UITools.dip2px(mCtx,100);
                mTextBottom = i * top + viewHeight + (i) * viewHeight + UITools.dip2px(mCtx,10) ;

            }else {
                /**左侧文字**/
                mTextLeft = layoutWidth/2 - viewWidth /2 - UITools.dip2px(mCtx,70);
                mTextTop = i * top + (i) * viewHeight + UITools.dip2px(mCtx,10);
                mTextRight = layoutWidth/2 - viewWidth ;
                mTextBottom = i * top + viewHeight + (i) * viewHeight + UITools.dip2px(mCtx,10) ;
            }

            /**设置textView文本**/
            TextView textView = new TextView(mCtx);
            textView.setText(data[i]);
            textView.setGravity(Gravity.CENTER_VERTICAL);
 //           textView.setBackgroundColor(getResources().getColor(R.color.red));


            /**设置imageView**/
            ImageView imageView = new ImageView(mCtx);
            imageView.setImageDrawable(getResources().getDrawable(R.mipmap.icon_circle));

            /**动态设置当前进度**/
            if (i <= PROGRESS)
            {
                textView.setTextColor(getResources().getColor(R.color.red));
                imageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
            }

            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(200, 50);

            /**添加textView和image到布局中**/
            mLlLine.addView(imageView,layoutParams);
            imageView.layout(mLeft,mTop,mRight,mBottom);
            mLlLine.addView(textView,layoutParams);
            textView.setGravity(View.TEXT_ALIGNMENT_CENTER);
            textView.layout(mTextLeft,mTextTop,mTextRight,mTextBottom);
        }
    }

    /**
     * 设置进度
     * @return
     */
    public void getProgress() {
        //TODO 设置数据
        PROGRESS = 5;
    }
}
