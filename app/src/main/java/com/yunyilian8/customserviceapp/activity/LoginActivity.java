package com.yunyilian8.customserviceapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tencent.connect.UserInfo;
import com.tencent.connect.common.Constants;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.yunyilian8.customserviceapp.AppInfo;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseActivity;
import com.yunyilian8.customserviceapp.bean.ResultBean;
import com.yunyilian8.customserviceapp.bean.UserInfoBean;
import com.yunyilian8.customserviceapp.service.RetrofitService;
import com.yunyilian8.customserviceapp.utils.Contants;
import com.yunyilian8.customserviceapp.utils.JSONUtils;
import com.yunyilian8.customserviceapp.utils.MD5Utils;
import com.yunyilian8.customserviceapp.utils.SerializeManager;
import com.yunyilian8.customserviceapp.utils.ToastUtil;
import com.yunyilian8.customserviceapp.view.TitleBarLayout;
import com.yunyilian8.customserviceapp.wxapi.AllApk;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import rx.Subscriber;

/**
 * 登录页面
 */

public class LoginActivity extends BaseActivity implements IWXAPIEventHandler{
    @InjectView(R.id.titlebar_layout)
    TitleBarLayout mTitlebarLayout;
    @InjectView(R.id.et_phone)
    EditText mEtPhone;
    @InjectView(R.id.et_password)
    EditText mEtPassword;
    @InjectView(R.id.tv_login)
    TextView mTvLogin;
    @InjectView(R.id.tv_forget_password)
    TextView mTvForgetPassword;
    @InjectView(R.id.tv_code_login)
    TextView mTvCodeLogin;
    @InjectView(R.id.ll_qq)
    LinearLayout mLlQq;
    @InjectView(R.id.ll_weChat)
    LinearLayout mLlWeChat;

    private static Tencent mTencent;
    private UserInfo mInfo;
    /** IWXAPI 是第三方app和微信通信的openapi接口 */
    private static boolean isServerSideLogin = false;
    /**微信第三方相关**/
    public static IWXAPI api;
    /**
     * 默认手机号输入不合法
     **/
    private boolean INPUT_PHONE = false;
    /**
     * 默认密码01没有输入
     **/
    private boolean INPUT_PASSWORD = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        mTencent = Tencent.createInstance("123456", this.getApplicationContext());

        setTitleBarListener();
        /**设置EditText的监听**/
        setEditTextListener();

    }


    /**
     * 设置EditText的监听
     */
    private void setEditTextListener() {
        /**电话EditText的监听**/
/*        mEtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (MatchUtil.isMobileNO(charSequence.toString().trim())) {

                    INPUT_PHONE = true;
                } else {
                    *//**手机号码输入不合法**//*
                    INPUT_PHONE = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/

        /**密码EditText的监听**/
        mEtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    INPUT_PASSWORD = true;
                } else {
                    INPUT_PASSWORD = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    /**
     *
     */
    private void setTitleBarListener() {
        mTitlebarLayout.setTitleBarListener(new TitleBarLayout.TitleBarListener() {
            @Override
            public void onBackClick() {

            }

            @Override
            public void onActionImageClick() {

            }

            @Override
            public void onActionClick() {
                /**去注册**/
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    @OnClick({ R.id.tv_login, R.id.tv_forget_password, R.id.tv_code_login, R.id.ll_qq, R.id.ll_weChat})
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_login:
                /**登录**/
                login();
                break;
            case R.id.tv_forget_password:
                /**忘记密码**/
                intent = new Intent(LoginActivity.this,ForgetPasswordActivity.class);
                intent.putExtra("type", 1);
                startActivity(intent);
                break;
            case R.id.tv_code_login:
                /**验证码登录**/
                intent = new Intent(LoginActivity.this,ForgetPasswordActivity.class);
                intent.putExtra("type", 2);
                startActivity(intent);
                break;
            case R.id.ll_qq:
                /**qq登录**/
                loginQQ();
                break;
            case R.id.ll_weChat:
                /**微信登录**/
                regToWx();
                if(!api.isWXAppInstalled()){
                    ToastUtil.showToast(mAppContext,"请安装微信客户端之后再进行登录");
                    return;
                }
                getCode();
                break;
        }
    }

    /**
     * 登录
     */
    private void login() {
        String phoneNum = mEtPhone.getText().toString().trim();
        String password = mEtPassword.getText().toString().trim();
     /* if (!INPUT_PHONE)
        {
            ToastUtil.showToast(mAppContext,"手机号码不合法");
            return;
        }*/
        if (!INPUT_PASSWORD)
        {
            ToastUtil.showToast(mAppContext,"请输入密码");
            return;
        }
        login2Net(phoneNum,password,true);
    }

    /**
     * 去网络登录
     * @param phoneNum
     * @param password
     */
    private void login2Net(String phoneNum, String password,boolean showDialog) {
        //TODO 去网络登录
        password = MD5Utils.GetMD5Code(password + RetrofitService.passwordMD5);

        Log.d("password",password);
        loadDataFromNet(new Subscriber<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {
                hideLoadingView();
                ToastUtil.showToast(mAppContext,"登录失败！");
            }

            @Override
            public void onNext(String result) {
                hideLoadingView();
                try {
                    if (ResultBean.SUCCESSfUL.equals(JSONUtils.getValue(result,"status"))) {

                        /**处理数据**/
                        processData(result);
                    } else {
                        ToastUtil.showToast(mAppContext, "登录失败");
                    }
                } catch (Exception e) {
                }
            }
        }, RetrofitService.getInstance().loginUser(phoneNum,password));
        if (showDialog)
            showLoadingView();
    }

    /**
     *
     * @param result
     */
    private void processData(String result) {
        UserInfoBean bean = JSONUtils.getObject(result, "user", UserInfoBean.class);
        AppInfo.mInfo = bean;
        /**将用户信息存入到本地，用于实现默认登录**/
        SerializeManager.saveFile(bean, Contants.UserInfo);

        /**进入主页**/
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finishToNewActivity();
    }


    /** -------------------------微信第三方登录---------------------- */
    /**
     *
     * 微信平台应用授权登录接入代码示例
     *
     * */
    private void regToWx(){
        // 通过WXAPIFactory工厂,获得IWXAPI的实例
        api = WXAPIFactory.createWXAPI(this, AllApk.WEIXIN_APP_ID, true);
        // 将应用的appid注册到微信
        api.registerApp(AllApk.WEIXIN_APP_ID);
    }
    //获取微信访问getCode
    private void getCode(){
        final SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "carjob_wx_login";
        api.sendReq(req);
    }
    /** -------------------------微信第三方登录结束-------------------- */

    /** ------------------------QQ第三方登录-------------------- */
    public void loginQQ(){
        /** 判断是否登陆过 */
        if (!mTencent.isSessionValid()){
            mTencent.login(this, "all",loginListener);
        }/** 登陆过注销之后在登录 */
        else {
            mTencent.logout(this);
            mTencent.login(this, "all",loginListener);
        }
    }
    IUiListener loginListener = new BaseUiListener() {
        @Override
        protected void doComplete(JSONObject values) {
            initOpenidAndToken(values);
            updateUserInfo();
        }
    };
    /** QQ登录第二步：存储token和openid */
    public static void initOpenidAndToken(JSONObject jsonObject) {
        try {
            String token = jsonObject.getString(Constants.PARAM_ACCESS_TOKEN);
            String expires = jsonObject.getString(Constants.PARAM_EXPIRES_IN);
            String openId = jsonObject.getString(Constants.PARAM_OPEN_ID);
            if (!TextUtils.isEmpty(token) && !TextUtils.isEmpty(expires) && !TextUtils.isEmpty(openId)) {
                mTencent.setAccessToken(token, expires);
                mTencent.setOpenId(openId);
            }
        } catch(Exception e) {
        }
    }
    /** QQ登录第三步：获取用户信息 */
    private void updateUserInfo() {
        if (mTencent != null && mTencent.isSessionValid()) {
            IUiListener listener = new IUiListener() {
                @Override
                public void onError(UiError e) {
                    Message msg = new Message();
                    msg.obj = "把手机时间改成获取网络时间";
                    msg.what = 1;
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onComplete(final Object response) {
                    Message msg = new Message();
                    msg.obj = response;
                    msg.what = 0;
                    mHandler.sendMessage(msg);
                }
                @Override
                public void onCancel() {
                    Message msg = new Message();
                    msg.obj = "获取用户信息失败";
                    msg.what = 2;
                    mHandler.sendMessage(msg);
                }
            };
            mInfo = new UserInfo(this, mTencent.getQQToken());
            mInfo.getUserInfo(listener);
        } else {

        }
    }
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            /** 获取用户信息成功 */
            if (msg.what == 0) {
                JSONObject response = (JSONObject) msg.obj;
                if (response.has("nickname")) {
                  ToastUtil.showToast(mCtx,"获取用户信息成功，返回结果："+response.toString());
                }
            }else if(msg.what == 1){

                ToastUtil.showToast(mCtx,msg+"");
            }else if(msg.what == 2){
                ToastUtil.showToast(mCtx,msg+"");
            }
        }

    };

    @Override
    public void onReq(BaseReq baseReq) {

    }

    @Override
    public void onResp(BaseResp baseResp) {

    }

    /** QQ登录第一步：获取token和openid */
    private class BaseUiListener implements IUiListener {
        @Override
        public void onComplete(Object response) {
            if (null == response) {
                ToastUtil.showToast(mCtx,"登录失败");
                return;
            }
            JSONObject jsonResponse = (JSONObject) response;
            if (null != jsonResponse && jsonResponse.length() == 0) {
                ToastUtil.showToast(mCtx,"登录失败");
                return;
            }
            ToastUtil.showToast(mCtx,"QQ登录成功返回结果-" + response.toString());
            doComplete((JSONObject)response);
        }
        protected void doComplete(JSONObject response) {}
        @Override
        public void onError(UiError e) {
            ToastUtil.showToast(mCtx,"错误");
        }
        @Override
        public void onCancel() {
            ToastUtil.showToast(mCtx,"取消");
            if (isServerSideLogin) {
                isServerSideLogin = false;
            }
        }
    }
    /** -------------------------QQ第三方登录结束-------------------- */
}
