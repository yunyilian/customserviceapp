package com.yunyilian8.customserviceapp.activity;

import android.os.Bundle;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseActivity;

/**
 * 项目名称页面
 */

public class ProjectNameActivity extends BaseActivity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
    }
}
