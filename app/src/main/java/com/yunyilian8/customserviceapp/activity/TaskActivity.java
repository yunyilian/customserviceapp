package com.yunyilian8.customserviceapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.adapter.TaskListViewAdapter;
import com.yunyilian8.customserviceapp.base.BaseActivity;
import com.yunyilian8.customserviceapp.bean.AnsweredTaskBean;
import com.yunyilian8.customserviceapp.service.RetrofitService;
import com.yunyilian8.customserviceapp.utils.ToastUtil;
import com.yunyilian8.customserviceapp.view.TitleBarLayout;
import com.yunyilian8.customserviceapp.view.kalistview.SmoothListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;


/**
 * （推过来的任务，我发布的，我接的）的listView  界面
 */

public class TaskActivity extends BaseActivity implements SmoothListView.ISmoothListViewListener, AdapterView.OnItemClickListener {

    private List<AnsweredTaskBean> mDataList=new ArrayList<AnsweredTaskBean>();

    private SmoothListView mListView;
    /**当前数据的页数，默认一页**/
    private int mPage = 1;
    private int mType;
    private TaskListViewAdapter mAdapter;
    private int SELECTION;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        ButterKnife.inject(this);

        initView();
    }

    private void initView() {
        TitleBarLayout titlebar_layout= (TitleBarLayout) findViewById(R.id.titlebar_layout);
        mListView=(SmoothListView)findViewById(R.id.slv_task);

        mListView.setSmoothListViewListener(this);
        mListView.setOnItemClickListener(this);
        mListView.setRefreshEnable(true);
        mListView.setLoadMoreEnable(true);

        Intent intent = getIntent();
        mType = intent.getIntExtra("type",-1);
        switch (mType){
            case 1:
                titlebar_layout.setTitleText("推过来的任务");
                initData();
                break;
            case 2:
                titlebar_layout.setTitleText("我发布的");
                initData();
                break;
            case 3:
                titlebar_layout.setTitleText("我接的");
                initData();
                break;
            default:
                break;
        }
    }

    /**
     *推过来的任务Data
     */
    private void initData() {

        for(int i=0;i<RetrofitService.PAGE_SIZE;i++){
            AnsweredTaskBean bean = new AnsweredTaskBean();
            bean.setType(1);
            bean.setImageURL("http://tupian.qqjay.com/tou2/2016/0830/d5a4c03f3bb1d3b15a518ad69ea04394.jpg");
            bean.setName("王彦斌" + i);
            bean.setContent("你好啊范德萨范德萨范德萨" + i);
            bean.setPraiseNum("9");
            bean.setCommentNum("124");
            bean.setDayNum("12");
            bean.setProgress(85);

            mDataList.add(bean);
        }
        SELECTION = mDataList.size();
        mAdapter = new TaskListViewAdapter(TaskActivity.this,mDataList,mType);
        mListView.setAdapter(mAdapter);

    }


    /**
     * 从服务器获取数据
     */
    private void loadData(int page) {
        //TODO 从服务器获取数据
        ToastUtil.showToast(mAppContext,page+"");
        if (1 == page)
        {
            mDataList.clear();
            /**刷新数据**/
            for(int i=0;i< RetrofitService.PAGE_SIZE;i++){
                AnsweredTaskBean bean = new AnsweredTaskBean();
                bean.setType(1);
                bean.setImageURL("http://tupian.qqjay.com/tou2/2016/0830/d5a4c03f3bb1d3b15a518ad69ea04394.jpg");
                bean.setName("新数据-王彦斌" + i);
                bean.setContent("新数据-你好啊范德萨范德萨范德萨" + i);
                bean.setPraiseNum("10");
                bean.setCommentNum("124");
                bean.setDayNum("12");
                bean.setProgress(85);

                mDataList.add(bean);
            }
            mAdapter.setList(mDataList);
            mListView.setAdapter(mAdapter);
            mListView.stopRefresh();

        }else {
            /**新增数据**/
            for(int i=0;i<RetrofitService.PAGE_SIZE;i++){
                AnsweredTaskBean bean = new AnsweredTaskBean();
                bean.setType(1);
                bean.setImageURL("http://tupian.qqjay.com/tou2/2016/0830/d5a4c03f3bb1d3b15a518ad69ea04394.jpg");
                bean.setName("更多数据-王彦斌" + i);
                bean.setContent("更多数据-你好啊范德萨范德萨范德萨" + i);
                bean.setPraiseNum("10");
                bean.setCommentNum("124");
                bean.setDayNum("12");
                bean.setProgress(21);

                mDataList.add(bean);
            }
            mAdapter.setList(mDataList);
            mListView.setAdapter(mAdapter);

            mListView.setSelection(SELECTION);
            SELECTION = mDataList.size();
            mListView.stopLoadMore();
        }

    }

    @Override
    public void onRefresh() {
        mPage = 1;
        loadData(mPage);
    }

    @Override
    public void onLoadMore() {
        ToastUtil.showToast(mAppContext,mPage+"");
        loadData(++ mPage);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ToastUtil.showToast(mAppContext,"type:" + mType + ":::item:" + i);
        Intent intent=new Intent(this,PushDetailsActivity.class);
        startActivity(intent);
    }

}

