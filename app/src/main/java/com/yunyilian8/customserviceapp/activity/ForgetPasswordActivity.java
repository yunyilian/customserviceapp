package com.yunyilian8.customserviceapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseActivity;
import com.yunyilian8.customserviceapp.utils.MatchUtil;
import com.yunyilian8.customserviceapp.utils.ToastUtil;
import com.yunyilian8.customserviceapp.view.TitleBarLayout;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * 忘记密码/验证码登录 页面
 * 根据传递过来的不同type值，来动态设置布局以及逻辑。
 */

public class ForgetPasswordActivity extends BaseActivity {
    @InjectView(R.id.et_phone)
    EditText mEtPhone;
    @InjectView(R.id.et_code)
    EditText mEtCode;
    @InjectView(R.id.tv_send)
    TextView mTvSend;
    @InjectView(R.id.et_password)
    EditText mEtPassword;
    @InjectView(R.id.tv_complete)
    TextView mTvComplete;
    @InjectView(R.id.titlebar_layout)
    TitleBarLayout mTitlebarLayout;

    /**
     * 验证码倒计时部分
     **/
    private int mCount = 61;
    private Timer mReSendCodeTimer;
    /**
     * 默认不可以获取验证码
     **/
    private boolean GET_CODE = false;
    /**
     * 默认手机号输入不合法
     **/
    private boolean INPUT_PHONE = false;
    /**
     * 默认验证码没有输入
     **/
    private boolean INPUT_CODE = false;
    /**
     * 默认密码没有输入
     **/
    private boolean INPUT_PASSWORD = false;
    private int mType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.inject(this);

        /**获取type值**/
        initType();

        /**设置EditText的监听**/
        setEditTextListener();
    }

    /**
     * 获取type值
     */
    private void initType() {
        Intent intent = getIntent();
        mType = intent.getIntExtra("type", -1);

        initView();
    }

    /**
     * 根据type，初始化布局
     */
    private void initView() {
        if (1 == mType) {
            /**忘记密码**/
            mTitlebarLayout.setTitleText("忘记密码");
        } else if (2 == mType) {
            /**验证码登录**/
            mTitlebarLayout.setTitleText("验证码登录");
            mTvComplete.setText("登录");
            mEtPassword.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.tv_send, R.id.tv_complete})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_send:
                /**获取验证码**/
                getCode();
                break;
            case R.id.tv_complete:
                /**提交**/
                goToSubmit();
                break;
        }
    }

    /**
     * 验证提交
     */
    private void goToSubmit() {
        if (!INPUT_CODE) {
            ToastUtil.showToast(mAppContext, "请输入验证码!");
            return;
        } else if (!INPUT_PASSWORD && 1 == mType) {
            ToastUtil.showToast(mAppContext, "请输入密码!");
            return;
        }

        /**提交数据**/
        String phone = mEtPhone.getText().toString().trim();
        String code = mEtCode.getText().toString().trim();
        String password = mEtPassword.getText().toString().trim();

        /**请求服务器提交数据**/
        submitOfNet(phone, code, password);
    }

    /**
     * @param phone    电话号码
     * @param code     验证码
     * @param password 密码
     */
    private void submitOfNet(String phone, String code, String password) {
        //TODO 提交数据
        if (1 == mType) {
            ToastUtil.showToast(mAppContext, phone + ": " + code + ": " + password);
        } else if (2 == mType) {
            ToastUtil.showToast(mAppContext, phone + ": " + code);
        }

    }

    /**
     * 设置EditText的监听
     */
    private void setEditTextListener() {
        /**电话EditText的监听**/
        mEtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (MatchUtil.isMobileNO(charSequence.toString().trim())) {
                    mTvSend.setBackgroundResource(R.drawable.selector_shape_yellow_yellow);
                    mTvSend.setTextColor(getResources().getColor(R.color.white));
                    GET_CODE = true;
                    INPUT_PHONE = true;

                } else {
                    /**手机号码输入不合法**/
                    mTvSend.setBackgroundResource(R.drawable.selector_shape_gray_gray);
                    mTvSend.setTextColor(getResources().getColor(R.color.gray13));
                    INPUT_PHONE = false;
                    GET_CODE = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        /**验证码EditText的监听**/
        mEtCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    INPUT_CODE = true;
                } else {
                    INPUT_CODE = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        /**密码EditText的监听**/
        mEtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    INPUT_PASSWORD = true;
                } else {
                    INPUT_PASSWORD = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    /**
     * 获取验证码
     */
    public void getCode() {
        if (GET_CODE) {
            /**表示可以获取验证码**/
            String phone = mEtPhone.getText().toString().trim();
            /**去服务器获取验证码**/
            getCodeFromNet(phone);
        } else {
            ToastUtil.showToast(mAppContext, "手机输入不合法!");
        }
    }

    /**
     * 去服务器获取验证码
     *
     * @param phone
     */
    private void getCodeFromNet(String phone) {
        //TODO 去服务器获取验证码
        ToastUtil.showToast(mAppContext, "去网络验证验证码");

        /**验证码倒计时**/
        startCheckCodeTimer();
    }

    /**
     * 倒计时逻辑
     */
    private void startCheckCodeTimer() {
        stopCheckCodeTimer();

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                mTvSend.setText(String.format("%ss", mCount + ""));
                if (mCount < 2) {
                    stopCheckCodeTimer();
                    mTvSend.setEnabled(true);
                    mTvSend.setText("获取验证码");
                }
            }
        };
        mReSendCodeTimer = new Timer();
        mTvSend.setEnabled(false);
        mReSendCodeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mCount--;
                handler.sendEmptyMessage(0);
            }
        }, 0, 1000);
    }

    private void stopCheckCodeTimer() {
        if (null != mReSendCodeTimer) {
            mReSendCodeTimer.cancel();
            mReSendCodeTimer = null;
            mCount = 61;
        }
    }
}
