package com.yunyilian8.customserviceapp.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.AppInfo;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.adapter.HomeViewPagerAdapter;
import com.yunyilian8.customserviceapp.base.BaseFragmentActivity;
import com.yunyilian8.customserviceapp.fragment.DevelopmentEntryDetailAdvantageFragment;
import com.yunyilian8.customserviceapp.fragment.DevelopmentEntryDetailClassificationFragment;
import com.yunyilian8.customserviceapp.fragment.DevelopmentEntryDetailRuleFragment;
import com.yunyilian8.customserviceapp.view.CustomViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * 详情页面（开发者入驻）
 */

public class DevelopmentEntryDetailActivity extends BaseFragmentActivity {

    @InjectView(R.id.tv_advantage)
    TextView tv_advantage;
    @InjectView(R.id.tv_classification)
    TextView tv_classification;
    @InjectView(R.id.tv_rule)
    TextView tv_rule;
    @InjectView(R.id.cvp_viewPager)
    CustomViewPager mViewPager;

    /**
     * fragment管理器
     **/
    private FragmentManager mFragmentManager;
    /**
     * 用于填充viewPager的list数据
     **/
    private List<Fragment> mFragmentList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppInfo.addActivity(this);
        setContentView(R.layout.activity_development_entry_detail);
        ButterKnife.inject(this);


        /**初始化数据**/
        initData();
        /**初始化fragments**/
        initFragments();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        //TODO 初始化数据
    }

    /**
     * 初始化fragments
     */
    private void initFragments() {
        try {
            mFragmentManager = this.getSupportFragmentManager();

            /**“分类”页面**/
            mFragmentList.add(new DevelopmentEntryDetailClassificationFragment());

            /**“优势”界面**/
            mFragmentList.add(new DevelopmentEntryDetailAdvantageFragment());

            /**“规则”界面**/
            mFragmentList.add(new DevelopmentEntryDetailRuleFragment());
            mViewPager.setAdapter(new HomeViewPagerAdapter(mFragmentList, mFragmentManager));
            mViewPager.setCurrentItem(0);
            mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    reSetColors();
                    switch (position) {
                        case 0:
                            tv_classification.setTextColor(getResources().getColor(R.color.red));
                            break;
                        case 1:
                            tv_advantage.setTextColor(getResources().getColor(R.color.red));
                            break;
                        case 2:
                            tv_rule.setTextColor(getResources().getColor(R.color.red));
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.tv_classification, R.id.tv_advantage, R.id.tv_rule})
    public void onClick(View view) {
        reSetColors();
        switch (view.getId()) {
            case R.id.tv_classification:
                /**分类**/
                mViewPager.setCurrentItem(0);
                tv_classification.setTextColor(getResources().getColor(R.color.red));
                break;
            case R.id.tv_advantage:
                /**优势**/
                mViewPager.setCurrentItem(1);
                tv_advantage.setTextColor(getResources().getColor(R.color.red));
                break;
            case R.id.tv_rule:
                /**规则**/
                mViewPager.setCurrentItem(2);
                tv_rule.setTextColor(getResources().getColor(R.color.red));
                break;
        }
    }

    private void reSetColors() {
        tv_classification.setTextColor(getResources().getColor(R.color.gray15));
        tv_advantage.setTextColor(getResources().getColor(R.color.gray15));
        tv_rule.setTextColor(getResources().getColor(R.color.gray15));
    }


}