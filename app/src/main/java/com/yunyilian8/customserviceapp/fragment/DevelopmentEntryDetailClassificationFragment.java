package com.yunyilian8.customserviceapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.adapter.ClassificationListViewAdapter;
import com.yunyilian8.customserviceapp.base.BaseFragment;
import com.yunyilian8.customserviceapp.bean.DevelopmentEntryDetailClassificationBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * 分类（开发者入驻详情）
 */

public class DevelopmentEntryDetailClassificationFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(mAppContext, R.layout.fragment_development_enty_detail_classification, null);
        ButterKnife.inject(this, view);

        ListView listView = (ListView) view.findViewById(R.id.lv_classification);
        listView.setDividerHeight(0);
        listView.setDivider(null);
        List<DevelopmentEntryDetailClassificationBean> developmentEntryDetailClassificationBean = new ArrayList<DevelopmentEntryDetailClassificationBean>();

        for (int i = 0; i < 3; i++) {
            DevelopmentEntryDetailClassificationBean p = new DevelopmentEntryDetailClassificationBean();
            p.setTv_personnel("标题" + i);
            p.setTv_tool_language("描述" + i);
            developmentEntryDetailClassificationBean.add(p);
        }

        listView.setAdapter(new ClassificationListViewAdapter(getContext(), developmentEntryDetailClassificationBean));
        return view;
    }
}
