package com.yunyilian8.customserviceapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.activity.DevelopmentEntryDetailActivity;
import com.yunyilian8.customserviceapp.activity.ProjectDevelopmentActivity;
import com.yunyilian8.customserviceapp.activity.TaskActivity;
import com.yunyilian8.customserviceapp.base.BaseFragment;
import com.yunyilian8.customserviceapp.utils.AnyEventType;
import com.yunyilian8.customserviceapp.utils.ToastUtil;
import com.yunyilian8.customserviceapp.view.SettingItemLayout;
import com.yunyilian8.customserviceapp.view.XCRoundImageView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2016/12/13.
 * 我的fragment
 */

public class HomeMyFragment extends BaseFragment {

    @InjectView(R.id.iv_title)
    XCRoundImageView mIvTitle;
    @InjectView(R.id.tv_name)
    TextView mTvName;
    @InjectView(R.id.sil_01)
    SettingItemLayout mSil01;
    @InjectView(R.id.sil_02)
    SettingItemLayout mSil02;
    @InjectView(R.id.sil_03)
    SettingItemLayout mSil03;
    @InjectView(R.id.sil_04)
    SettingItemLayout mSil04;
    @InjectView(R.id.sil_05)
    SettingItemLayout mSil05;
    @InjectView(R.id.sil_06)
    SettingItemLayout mSil06;
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        mView = inflater.inflate(R.layout.fragment_home_my, null);
        ButterKnife.inject(this, mView);

        return mView;
    }

    /**
     * EventBus方法
     */
    public void onEventMainThread(AnyEventType event) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick({R.id.iv_title, R.id.sil_01, R.id.sil_02, R.id.sil_03, R.id.sil_04, R.id.sil_05, R.id.sil_06})
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.iv_title:
                /**头像**/
                ToastUtil.showToast(mAppContext,"头像");
                break;
            case R.id.sil_01:
                /**推过来的任务**/
                ToastUtil.showToast(mAppContext,"推过来的任务");
                intent = new Intent(this.getActivity(), TaskActivity.class);
                intent.putExtra("type",1);
                startActivity(intent);
                break;
            case R.id.sil_02:
                /**我发布的**/
                ToastUtil.showToast(mAppContext,"我发布的");
                intent = new Intent(this.getActivity(), TaskActivity.class);
                intent.putExtra("type",2);
                startActivity(intent);
                break;
            case R.id.sil_03:
                /**我接的**/
                ToastUtil.showToast(mAppContext,"我接的");
                intent = new Intent(this.getActivity(), TaskActivity.class);
                intent.putExtra("type",3);
                startActivity(intent);
                break;
            case R.id.sil_04:
                /**开发者入驻**/
                ToastUtil.showToast(mAppContext,"开发者入驻");
                intent = new Intent(this.getActivity(), DevelopmentEntryDetailActivity.class);
                startActivity(intent);
                break;
            case R.id.sil_05:
                /**我的账户**/
                ToastUtil.showToast(mAppContext,"我的账户");
                break;
            case R.id.sil_06:
                /**查看项目流程**/
                ToastUtil.showToast(mAppContext,"查看项目流程");
                intent = new Intent(this.getActivity(), ProjectDevelopmentActivity.class);
                startActivity(intent);
                break;
        }
    }
}
