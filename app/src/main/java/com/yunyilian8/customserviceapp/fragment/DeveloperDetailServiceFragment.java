package com.yunyilian8.customserviceapp.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.adapter.TagAdapter;
import com.yunyilian8.customserviceapp.adapter.TagAdapter02;
import com.yunyilian8.customserviceapp.base.BaseFragment;
import com.yunyilian8.customserviceapp.bean.DevelopmentSchemeRecyclerBean;
import com.yunyilian8.customserviceapp.utils.AnyEventType;
import com.yunyilian8.customserviceapp.utils.EventBusEvents;
import com.yunyilian8.customserviceapp.utils.ToastUtil;
import com.yunyilian8.customserviceapp.view.flowtaglayout.FlowTagLayout;
import com.yunyilian8.customserviceapp.view.flowtaglayout.FlowTagLayout02;
import com.yunyilian8.customserviceapp.view.flowtaglayout.OnTagSelectListener02;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2016/12/15.
 * 开发者详情-提供的服务fragment页面
 */

public class DeveloperDetailServiceFragment extends BaseFragment {
    @InjectView(R.id.tv_price)
    TextView mTvPrice;
    @InjectView(R.id.btn_delete)
    Button mBtnDelete;
    @InjectView(R.id.tv_goods_num)
    TextView mTvGoodsNum;
    @InjectView(R.id.btn_add)
    Button mBtnAdd;
    @InjectView(R.id.tv_buy)
    TextView mTvBuy;
    @InjectView(R.id.tv_tell)
    TextView mTvTell;
    @InjectView(R.id.size_flow_layout01)
    FlowTagLayout mLayout01;
    @InjectView(R.id.size_flow_layout02)
    FlowTagLayout02 mLayout02;

    /**
     * 默认购买商品数量为1
     **/
    private int GOODS_NUM = 1;
    /**
     * 展示所有服务adapter
     **/
    private TagAdapter mSizeTagAdapter01;
    /**
     * 选择需要的服务adapter
     **/
    private TagAdapter02 mSizeTagAdapter02;

    private int number=1;
    private int chooseService=0;
    private String price;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(mAppContext, R.layout.fragment_developer_detail_service, null);
        ButterKnife.inject(this, view);
        EventBus.getDefault().register(this);

        /**展示所有服务**/
        initServiceFlowData();
        /**选择需要的服务**/
        initServiceChooseFlowData();

        return view;
    }


    /**
     * 接受事件广播
     */
    public void onEventMainThread(AnyEventType event) {
        ToastUtil.showToast(mAppContext, "0KOKOKOK");
        switch (event.getType()) {
            case EventBusEvents.EVENT_BROADCAST_DEVELOPER_DETAIL:

                /**开发者-详细信息页面activity传递过来的数据**/
                DevelopmentSchemeRecyclerBean bean = (DevelopmentSchemeRecyclerBean) event.getObj();
                //TODO
                break;
        }
    }

    /**
     * 选择需要的服务
     */
    private void initServiceChooseFlowData() {
        mSizeTagAdapter02 = new TagAdapter02<>(getActivity());
        mLayout02.setTagCheckedMode(FlowTagLayout.FLOW_TAG_CHECKED_SINGLE);
        mLayout02.setAdapter(mSizeTagAdapter02);
        mLayout02.setOnTagSelectListener(new OnTagSelectListener02() {
            @Override
            public void onItemSelect(FlowTagLayout02 parent, List<Integer> selectedList,int position) {
                if (selectedList != null && selectedList.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    for (int i : selectedList) {
                        sb.append(parent.getAdapter().getItem(i));
                        sb.append(":");
                    }
                    ToastUtil.showToast(mAppContext, sb.toString());
                } else {
                    ToastUtil.showToast(mAppContext, "没有选择标签");
                }
                chooseService=position;
            }
        });

        initSizeData02();
    }

    /**
     * 展示所有服务
     */
    private void initServiceFlowData() {
        mSizeTagAdapter01 = new TagAdapter<>(getActivity(), TagAdapter.LIST_TYPE_SERVICE);
        mLayout01.setType(FlowTagLayout.LIST_TYPE_SERVICE);
        mLayout01.setAdapter(mSizeTagAdapter01);

        initSizeData01();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        EventBus.getDefault().unregister(this);
    }

    @OnClick({R.id.btn_delete, R.id.btn_add, R.id.tv_buy, R.id.tv_tell})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_delete:
                /**数量减少**/
                GOODS_NUM--;
                if (GOODS_NUM <= 1) {
                    GOODS_NUM = 1;
                    mBtnDelete.setBackgroundResource(R.drawable.selector_commodity_details_rembg_02);
                    mTvGoodsNum.setText(GOODS_NUM + "");
                } else {
                    mBtnDelete.setBackgroundResource(R.drawable.selector_commodity_details_rembg);
                    mTvGoodsNum.setText(GOODS_NUM + "");
                }
                number=GOODS_NUM;
                break;
            case R.id.btn_add:
                /**数量增加**/
                if (GOODS_NUM >= 1) {
                    mBtnDelete.setBackgroundResource(R.drawable.selector_commodity_details_rembg);
                }
                mTvGoodsNum.setText(++GOODS_NUM + "");
                break;
            case R.id.tv_buy:
                /**立即购买**/
                ToastUtil.showToast(mAppContext,"立即购买");
                toBuy();
                break;
            case R.id.tv_tell:
                /**拨打电话**/
                ToastUtil.showToast(mAppContext,"拨打电话");
                toPhone("18157421808");
                break;
        }
    }


    /**
     * 初始化数据
     */
    private void initSizeData01() {
        List<String> dataSource = new ArrayList<>();
        dataSource.add("android开发");
        dataSource.add("IOS开发");
        dataSource.add("UI设计");
        dataSource.add("AR开发");
        mSizeTagAdapter01.onlyAddAll(dataSource);

    }

    /**
     * 初始化数据
     */
    private void initSizeData02() {
        List<String> dataSource = new ArrayList<>();
        dataSource.add("android开发");
        dataSource.add("IOS开发");
        dataSource.add("UI设计");
        dataSource.add("AR开发");
        mSizeTagAdapter02.onlyAddAll(dataSource);

    }

    /**立即购买**/
    private  void toBuy(){
        String str = mTvPrice.getText().toString();
        price = str.substring(1, str.indexOf("/"));
        Log.e("ssss",chooseService+"======"+number+"======"+price);
    }


    /**拨打电话**/
    private void toPhone(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
