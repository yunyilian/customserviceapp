package com.yunyilian8.customserviceapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseFragment;

import butterknife.ButterKnife;

/**
 * 优势（开发者入驻详情）
 */

public class DevelopmentEntryDetailAdvantageFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(mAppContext, R.layout.fragment_development_enty_detail_advantage, null);
        ButterKnife.inject(this, view);


        return view;
    }
}
