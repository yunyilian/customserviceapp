package com.yunyilian8.customserviceapp.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jude.rollviewpager.OnItemClickListener;
import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.activity.AcceptanceCheckPointActivity;
import com.yunyilian8.customserviceapp.activity.CustomDetailsActivity;
import com.yunyilian8.customserviceapp.activity.DevelopmentSchemeActivity;
import com.yunyilian8.customserviceapp.adapter.MyRecyclerviewAdapter;
import com.yunyilian8.customserviceapp.base.BaseFragment;
import com.yunyilian8.customserviceapp.bean.HomeBannerBean;
import com.yunyilian8.customserviceapp.service.RetrofitService;
import com.yunyilian8.customserviceapp.utils.AnyEventType;
import com.yunyilian8.customserviceapp.utils.ImageLoader;
import com.yunyilian8.customserviceapp.utils.JSONUtils;
import com.yunyilian8.customserviceapp.utils.NetUtil;
import com.yunyilian8.customserviceapp.utils.ToastUtil;
import com.yunyilian8.customserviceapp.utils.UITools;
import com.yunyilian8.customserviceapp.view.DividerGridItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import rx.Subscriber;

/**
 * Created by Administrator on 2016/12/13.
 * 软件外包对应fragment
 */

public class HomeCustomFragment extends BaseFragment {

    @InjectView(R.id.rpv_banner)
    RollPagerView mBanner;
    @InjectView(R.id.tv_type)
    TextView mTvType;
    @InjectView(R.id.tv_point)
    TextView mTvPoint;
    @InjectView(R.id.rv_grid)
    RecyclerView mRecyclerView;
    @InjectView(R.id.ll_layout)
    LinearLayout mLlLayout;
    private View mView;
    private TestNomalAdapter mNormalAdapter;
    private Handler handler = new Handler();
    private List<HomeBannerBean> bannerList = new ArrayList<>();
    private List<String> banner = new ArrayList<>();

    private List<Integer> mImageDatas = new ArrayList<>();


    /**
     * 每行的item数
     **/
    private static final int HORIZONTAL_NUM = 2;
    /**
     * item间距
     **/
    private static final int ITEM_MAGIN = 10;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        mView = inflater.inflate(R.layout.fragment_home_custom, null);
        ButterKnife.inject(this, mView);

        /**从网络获取轮播图数据**/
        loadBannerData(false);
        initDefaultImage();
        initImages();
        initViewPager();
        initRecyclerView();

//        mLlLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

        return mView;
    }

    /**
     * 获取轮播图数据
     */
    private void loadBannerData(final boolean showDialog) {
        {
            if (!NetUtil.checkNet(mCtx)) {
                ToastUtil.showToast(mAppContext, "网络没有连接！");
                return;
            }

            loadDataFromNet(new Subscriber<String>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable throwable) {
           //         ToastUtil.showToast(mAppContext, "获取数据失败！");
                }

                @Override
                public void onNext(String result) {

                    try {
                        if (result != null) {
                            bannerList.clear();
                            bannerList = JSONUtils.parseJson2List(result, HomeBannerBean.class);
                            if (null != bannerList)
                                processData(bannerList);
                        } else {
           //                 ToastUtil.showToast(mCtx, "获取数据失败");
                        }
                    } catch (Exception e) {
                    }
                }
            }, RetrofitService.getInstance().callBannerList());
            if (showDialog)
                showLoadingView();
        }
    }

    /**
     * 处理请求的数据
     *
     * @param bannerList
     */
    private void processData(List<HomeBannerBean> bannerList) {
        banner.clear();
        for (int i = 0; i < bannerList.size(); i++) {
            HomeBannerBean bean = bannerList.get(i);
            banner.add(bean.getImg_url());
        }
        /**设置数据成功后，设置到轮播图中**/
        setData();
    }

    private void initImages() {
        mImageDatas.add(R.mipmap.icon_ios);
        mImageDatas.add(R.mipmap.icon_android);
        mImageDatas.add(R.mipmap.icon_weixin);
        mImageDatas.add(R.mipmap.icon_web);
        mImageDatas.add(R.mipmap.icon_appweb);
        mImageDatas.add(R.mipmap.icon_hardware);
        mImageDatas.add(R.mipmap.icon_desktop);
        mImageDatas.add(R.mipmap.icon_vr);
    }

    private void initDefaultImage() {
        HomeBannerBean b = new HomeBannerBean();
        b.setImg_url("http://pic.58pic.com/58pic/11/20/89/67J58PICWep.jpg");
        bannerList.add(b);
        processData(bannerList);
    }


    /**
     * EventBus方法
     */
    public void onEventMainThread(AnyEventType event) {
    }

    /**
     * 初始化RecyclerView
     */
    private void initRecyclerView() {

        /**解决RecyclerView的滑动问题（不能流畅滑动的问题）**/
        UITools.setRecyclerViewScroll(mCtx, mRecyclerView);

        MyRecyclerviewAdapter adapter = new MyRecyclerviewAdapter(mImageDatas, mCtx);
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.setLayoutManager(new GridLayoutManager(mCtx, HORIZONTAL_NUM));
        /**设置分割线**/
        mRecyclerView.addItemDecoration(new DividerGridItemDecoration(mCtx, HORIZONTAL_NUM, ITEM_MAGIN, ITEM_MAGIN));

        //设置监听事件
        adapter.setOnItemClickListener(new MyRecyclerviewAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position, View view) {
                gotoVideoList(position, view);
            }

            @Override
            public void OnItemLongClick(int position, View view) {
                ToastUtil.showToast(mAppContext, position + "长安");
            }
        });
    }


    /**
     * 初始化viewPager
     */
    private void initViewPager() {
        /**设置顶部viewPager获得焦点,防止scrollView不在顶部的问题**/
        mBanner.setFocusable(true);
        mBanner.setFocusableInTouchMode(true);
        mBanner.requestFocus();

        mBanner.setPlayDelay(3000);
        mBanner.setAdapter(mNormalAdapter = new TestNomalAdapter());
        mBanner.setHintView(new ColorPointHintView(mCtx, Color.YELLOW, Color.WHITE));
        mBanner.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Toast.makeText(mAppContext, "Item " + position + " clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setData() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    final List<String> imgs = new ArrayList<>();
                    for (int i = 0; i < banner.size(); i++) {

                        imgs.add(banner.get(i));
                    }
                    Log.i("MainActivity", "new data:" + imgs.size());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mNormalAdapter.setImgs(imgs);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private class TestNomalAdapter extends StaticPagerAdapter {
        List<String> imgs = new ArrayList<>();

        public void setImgs(List<String> imgs) {
            this.imgs = imgs;
            notifyDataSetChanged();
        }

        @Override
        public View getView(ViewGroup container, int position) {
            ImageView view = new ImageView(container.getContext());
            view.setScaleType(ImageView.ScaleType.CENTER_CROP);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            /**加载图片**/
            ImageLoader.displayImage(mCtx, view, banner.get(position));
            return view;
        }

        @Override
        public int getCount() {
            return imgs.size();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick({R.id.tv_type, R.id.tv_point})
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_type:
                /**开发方式**/
                intent = new Intent(mCtx, DevelopmentSchemeActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_point:
                /**验收节点**/
                intent = new Intent(mCtx, AcceptanceCheckPointActivity.class);
                startActivity(intent);
                break;
        }
    }

    /**
     * 根据具体类型去对应的视频列表界面
     *
     * @param position
     * @param view
     */
    private void gotoVideoList(int position, View view) {
        Intent intent;
        switch (position) {
            case 0:
                /**IOS APP**/
                intent = new Intent(mCtx, CustomDetailsActivity.class);
                intent.putExtra("typeString", "IOS APP");
                intent.putExtra("typeInt", 0);
                startActivity(intent);
                break;
            case 1:
                /**安卓APP**/
                intent = new Intent(mCtx, CustomDetailsActivity.class);
                intent.putExtra("typeString", "安卓APP");
                intent.putExtra("typeInt", 1);
                startActivity(intent);
                break;
            case 2:
                /**微信公众号**/
                intent = new Intent(mCtx, CustomDetailsActivity.class);
                intent.putExtra("typeString", "微信公众号");
                intent.putExtra("typeInt", 2);
                startActivity(intent);
                break;
            case 3:
                /**Web 网站**/
                intent = new Intent(mCtx, CustomDetailsActivity.class);
                intent.putExtra("typeString", "Web 网站");
                intent.putExtra("typeInt", 3);
                startActivity(intent);
                break;
            case 4:
                /**手机网站**/
                intent = new Intent(mCtx, CustomDetailsActivity.class);
                intent.putExtra("typeString", "手机网站");
                intent.putExtra("typeInt", 4);
                startActivity(intent);
                break;
            case 5:
                /**智能硬件APP**/
                intent = new Intent(mCtx, CustomDetailsActivity.class);
                intent.putExtra("typeString", "智能硬件APP");
                intent.putExtra("typeInt", 5);
                startActivity(intent);
                break;
            case 6:
                /**桌面软件**/
                intent = new Intent(mCtx, CustomDetailsActivity.class);
                intent.putExtra("typeString", "桌面软件");
                intent.putExtra("typeInt", 6);
                startActivity(intent);
                break;
            case 7:
                /**VR APP**/
                intent = new Intent(mCtx, CustomDetailsActivity.class);
                intent.putExtra("typeString", "VR APP");
                intent.putExtra("typeInt", 7);
                startActivity(intent);
                break;
        }
    }
}
