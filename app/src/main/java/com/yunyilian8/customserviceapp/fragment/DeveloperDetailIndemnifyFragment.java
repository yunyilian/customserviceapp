package com.yunyilian8.customserviceapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseFragment;
import com.yunyilian8.customserviceapp.bean.DevelopmentSchemeRecyclerBean;
import com.yunyilian8.customserviceapp.utils.ImageLoader;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 服务保障fragment页面
 */

public class DeveloperDetailIndemnifyFragment extends BaseFragment {
    @InjectView(R.id.iv_image)
    ImageView mIvImage;
    private DevelopmentSchemeRecyclerBean mBean;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = View.inflate(mAppContext, R.layout.fragment_developer_detail_indemnify, null);
        ButterKnife.inject(this, view);

        getData();
        setData();
        setImageData();

        return view;
    }

    private void setData() {
        if (null != mBean) {
            String url = mBean.getURL();
            ImageLoader.displayGifImage(mCtx, mIvImage, url);
        }
    }


    /**
     * 设置数据
     */
    private void setImageData() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        EventBus.getDefault().unregister(this);
    }

    public void getData() {
        /**接受activity传递过来的数据**/
        mBean = getArguments().getParcelable("bean");
    }
}
