package com.yunyilian8.customserviceapp.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2016/12/22.
 */

public class HomeBannerBean implements Parcelable {


    /**
     * banner_id : 1
     * img_url : http://112.74.25.224/Upload/Banner/01-200x150.jpg
     * status : 0
     */

    private String banner_id;
    private String img_url;
    private String status;

    public String getBanner_id() {
        return banner_id;
    }

    public void setBanner_id(String banner_id) {
        this.banner_id = banner_id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.banner_id);
        dest.writeString(this.img_url);
        dest.writeString(this.status);
    }

    public HomeBannerBean() {
    }

    protected HomeBannerBean(Parcel in) {
        this.banner_id = in.readString();
        this.img_url = in.readString();
        this.status = in.readString();
    }

    public static final Parcelable.Creator<HomeBannerBean> CREATOR = new Parcelable.Creator<HomeBannerBean>() {
        @Override
        public HomeBannerBean createFromParcel(Parcel source) {
            return new HomeBannerBean(source);
        }

        @Override
        public HomeBannerBean[] newArray(int size) {
            return new HomeBannerBean[size];
        }
    };
}
