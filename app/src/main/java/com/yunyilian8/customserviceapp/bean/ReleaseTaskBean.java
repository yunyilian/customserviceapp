package com.yunyilian8.customserviceapp.bean;

/**
 * Created by whss on 2016/12/15.
 */

public class ReleaseTaskBean {
    private Integer img_id;
    private String tv_title;
    private String tv_describe;
    private String tv_phone;
    private String tv_weChat;
    private String tv_qq;
    private String tv_collection;
    private String tv_dustbin;
    private String tv_news;


    public ReleaseTaskBean() {
    }

    public Integer getImg_id() {
        return img_id;
    }

    public void setImg_id(Integer img_id) {
        this.img_id = img_id;
    }

    public String getTv_title() {
        return tv_title;
    }

    public void setTv_title(String tv_title) {
        this.tv_title = tv_title;
    }

    public String getTv_describe() {
        return tv_describe;
    }

    public void setTv_describe(String tv_describe) {
        this.tv_describe = tv_describe;
    }

    public String getTv_phone() {
        return tv_phone;
    }

    public void setTv_phone(String tv_phone) {
        this.tv_phone = tv_phone;
    }

    public String getTv_weChat() {
        return tv_weChat;
    }

    public void setTv_weChat(String tv_weChat) {
        this.tv_weChat = tv_weChat;
    }

    public String getTv_qq() {
        return tv_qq;
    }

    public void setTv_qq(String tv_qq) {
        this.tv_qq = tv_qq;
    }

    public String getTv_collection() {
        return tv_collection;
    }

    public void setTv_collection(String tv_collection) {
        this.tv_collection = tv_collection;
    }

    public String getTv_dustbin() {
        return tv_dustbin;
    }

    public void setTv_dustbin(String tv_dustbin) {
        this.tv_dustbin = tv_dustbin;
    }

    public String getTv_news() {
        return tv_news;
    }

    public void setTv_news(String tv_news) {
        this.tv_news = tv_news;
    }
}
