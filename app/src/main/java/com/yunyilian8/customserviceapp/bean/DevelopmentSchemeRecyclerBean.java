package com.yunyilian8.customserviceapp.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2016/12/15.
 */

public class DevelopmentSchemeRecyclerBean implements Parcelable {
    private String URL;
    private String name;
    private String know;
    private String price;

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKnow() {
        return know;
    }

    public void setKnow(String know) {
        this.know = know;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.URL);
        dest.writeString(this.name);
        dest.writeString(this.know);
        dest.writeString(this.price);
    }

    public DevelopmentSchemeRecyclerBean() {
    }

    protected DevelopmentSchemeRecyclerBean(Parcel in) {
        this.URL = in.readString();
        this.name = in.readString();
        this.know = in.readString();
        this.price = in.readString();
    }

    public static final Parcelable.Creator<DevelopmentSchemeRecyclerBean> CREATOR = new Parcelable.Creator<DevelopmentSchemeRecyclerBean>() {
        @Override
        public DevelopmentSchemeRecyclerBean createFromParcel(Parcel source) {
            return new DevelopmentSchemeRecyclerBean(source);
        }

        @Override
        public DevelopmentSchemeRecyclerBean[] newArray(int size) {
            return new DevelopmentSchemeRecyclerBean[size];
        }
    };
}
