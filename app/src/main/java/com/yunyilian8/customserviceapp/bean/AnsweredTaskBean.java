package com.yunyilian8.customserviceapp.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by whss on 2016/12/15.
 */

public class AnsweredTaskBean implements Parcelable {
    private int type;
    private String imageURL;
    private String name;
    private String content;
    private String praiseNum;
    private String commentNum;
    private String dayNum;
    private int progress;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPraiseNum() {
        return praiseNum;
    }

    public void setPraiseNum(String praiseNum) {
        this.praiseNum = praiseNum;
    }

    public String getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(String commentNum) {
        this.commentNum = commentNum;
    }

    public String getDayNum() {
        return dayNum;
    }

    public void setDayNum(String dayNum) {
        this.dayNum = dayNum;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeString(this.imageURL);
        dest.writeString(this.name);
        dest.writeString(this.content);
        dest.writeString(this.praiseNum);
        dest.writeString(this.commentNum);
        dest.writeString(this.dayNum);
        dest.writeInt(this.progress);
    }

    public AnsweredTaskBean() {
    }

    protected AnsweredTaskBean(Parcel in) {
        this.type = in.readInt();
        this.imageURL = in.readString();
        this.name = in.readString();
        this.content = in.readString();
        this.praiseNum = in.readString();
        this.commentNum = in.readString();
        this.dayNum = in.readString();
        this.progress = in.readInt();
    }

    public static final Parcelable.Creator<AnsweredTaskBean> CREATOR = new Parcelable.Creator<AnsweredTaskBean>() {
        @Override
        public AnsweredTaskBean createFromParcel(Parcel source) {
            return new AnsweredTaskBean(source);
        }

        @Override
        public AnsweredTaskBean[] newArray(int size) {
            return new AnsweredTaskBean[size];
        }
    };
}
