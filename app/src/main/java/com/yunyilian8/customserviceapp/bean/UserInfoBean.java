package com.yunyilian8.customserviceapp.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/12/27.
 * 用户信息封装bean
 */

public class UserInfoBean  implements Serializable {

    /**
     * address :
     * birthday : 0000-00-00
     * email :
     * id : 1
     * last_login_ip : 0
     * last_login_time : 1482819367
     * login : 14
     * mobile :
     * nickname :
     * password : 2f28a79d7752b588af5d467618ed3faf
     * position : 2
     * qq :
     * reg_ip : 0
     * reg_time : 0
     * score : 0
     * sex : 0
     * status : 0
     * update_time : 0
     * username : 100001
     */

    private String address;
    private String birthday;
    private String email;
    private String id;
    private String last_login_ip;
    private String last_login_time;
    private String login;
    private String mobile;
    private String nickname;
    private String password;
    private String position;
    private String qq;
    private String reg_ip;
    private String reg_time;
    private String score;
    private String sex;
    private String status;
    private String update_time;
    private String username;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLast_login_ip() {
        return last_login_ip;
    }

    public void setLast_login_ip(String last_login_ip) {
        this.last_login_ip = last_login_ip;
    }

    public String getLast_login_time() {
        return last_login_time;
    }

    public void setLast_login_time(String last_login_time) {
        this.last_login_time = last_login_time;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getReg_ip() {
        return reg_ip;
    }

    public void setReg_ip(String reg_ip) {
        this.reg_ip = reg_ip;
    }

    public String getReg_time() {
        return reg_time;
    }

    public void setReg_time(String reg_time) {
        this.reg_time = reg_time;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
