package com.yunyilian8.customserviceapp.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/12/27.
 */

public class ResultBean implements Serializable {

    public static final String SUCCESSfUL = "1";
    public static final String FAILED = "0";

    public String mTokenId = "";
    public String mDesc = "";
    public String mResultCode = FAILED;
    public String mReturnCode = "";
    public Object mObj;
}
