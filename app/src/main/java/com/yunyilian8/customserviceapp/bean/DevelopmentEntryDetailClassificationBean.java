package com.yunyilian8.customserviceapp.bean;

/**
 * Created by whss on 2016/12/19.
 */

public class DevelopmentEntryDetailClassificationBean {
    private Integer imageid;
    private String tv_personnel;
    private String tv_tool_language;

    public DevelopmentEntryDetailClassificationBean() {
    }

    public Integer getImageid() {
        return imageid;
    }

    public void setImageid(Integer imageid) {
        this.imageid = imageid;
    }

    public String getTv_personnel() {
        return tv_personnel;
    }

    public void setTv_personnel(String tv_personnel) {
        this.tv_personnel = tv_personnel;
    }

    public String getTv_tool_language() {
        return tv_tool_language;
    }

    public void setTv_tool_language(String tv_tool_language) {
        this.tv_tool_language = tv_tool_language;
    }
}
