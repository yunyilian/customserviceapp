package com.yunyilian8.customserviceapp.bean;

/**
 * Created by whss on 2016/12/15.
 */

public class PushTaskBean {
    private Integer img_id;
    private String tv_title;
    private String tv_describe;

    public PushTaskBean() {

    }

    public Integer getImg_id() {
        return img_id;
    }

    public void setImg_id(Integer img_id) {
        this.img_id = img_id;
    }

    public String getTv_describe() {
        return tv_describe;
    }

    public void setTv_describe(String tv_describe) {
        this.tv_describe = tv_describe;
    }

    public String getTv_title() {
        return tv_title;
    }

    public void setTv_title(String tv_title) {
        this.tv_title = tv_title;
    }
}
