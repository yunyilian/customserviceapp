package com.yunyilian8.customserviceapp.utils;

import android.os.Build;

/**
 * 版本工具
 */

public class VersionUtils {
    public static boolean aboveVersion21()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            /**api5.0及以上**/
            return true;
        }
        return false;
    }
}
