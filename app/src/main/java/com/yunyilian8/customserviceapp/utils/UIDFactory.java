package com.yunyilian8.customserviceapp.utils;

import java.util.UUID;

public class UIDFactory {

	public String mUid;
	
	public UIDFactory(){
		mUid = UUID.randomUUID().toString();
	}
	
	public String getUID(){
		return mUid;
	}
}
