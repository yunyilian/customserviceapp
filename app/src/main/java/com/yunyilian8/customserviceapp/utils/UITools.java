package com.yunyilian8.customserviceapp.utils;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * 类说明：
 *
 */

public class UITools {
    public static void hideSoftInput(View view) {
        final InputMethodManager im = (InputMethodManager) view.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static int[] getDeviceScreenWidthHeight(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int[] wh = new int[2];
        wh[0] = dm.widthPixels;
        wh[1] = dm.heightPixels;
        return wh;
    }

    /**
     * 该方法用于将dip大小转换为px大小
     *
     * @param context
     *            上下文对象
     * @param dipValue
     *            dip大小值
     * @return 转换后的px像素值
     */
    public static int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * 该方法用于将px大小转换为dip大小
     *
     * @param context
     *            上下文对象
     * @param pxValue
     *            px大小值
     * @return 转换后的dip值
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 该方法用于将sp大小转换为px大小
     *
     * @param context
     *            上下文对象
     * @param pxValue
     *            sp大小值
     * @return 转换后的dip值
     */
    public static int sp2px(Context context, float spValue) {
        final float scale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * scale + 0.5f);
    }

    /**
     * 获取地图中心点位置*/
    public static Point getMapCenterScreenPoint(Context context) {
        Point centerPoint = new Point();
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        centerPoint.x = dm.widthPixels/2;
        centerPoint.y = 80+(dm.heightPixels-240)/2+230;
        return centerPoint;
    }

    /**
     * 获取屏幕中心店位置*/
    public static Point getScreenCenterPoint(Context context) {
        Point centerPoint = new Point();
        int[] wh = getDeviceScreenWidthHeight(context);
        centerPoint.x = wh[0]/2;
        centerPoint.y = wh[1]/2;
        return centerPoint;
    }

    /**
     * 根据地图Marker点击位置获取地图中心位置
     * @param context
     * @param marker 地图上点击的marker
     * @param aMap
     * */
    /**
    public static LatLng getMapCenterLatLng(Context context,Marker marker,AMap aMap){
        Point screenPoint = UITools.getScreenCenterPoint(context);
        Point mapCenterPoint = UITools.getMapCenterScreenPoint(context);
        Point markerPoint = aMap.getProjection().toScreenLocation(marker.getPosition());
        Point newPoint = new Point();
        newPoint.x = markerPoint.x;
        newPoint.y = markerPoint.y-(mapCenterPoint.y-screenPoint.y);
        LatLng l= aMap.getProjection().fromScreenLocation(newPoint);
        return l;
    }
     */

    /**
     * 解决RecyclerView的滑动问题（不能流畅滑动的问题）
     * @param mCtx
     * @param mRecyclerView
     */
    public static void setRecyclerViewScroll(Context mCtx, RecyclerView mRecyclerView)
    {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mCtx);
        layoutManager.setSmoothScrollbarEnabled(true);
        layoutManager.setAutoMeasureEnabled(true);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
    }

    /**
     * 获得状态栏的高度
     *
     * @param context
     * @return
     */
    public static int getStatusHeight(Context context) {

        int statusHeight = -1;
        try {
            Class clazz = Class.forName("com.android.internal.R$dimen");
            Object object = clazz.newInstance();
            int height = Integer.parseInt(clazz.getField("status_bar_height")
                    .get(object).toString());
            statusHeight = context.getResources().getDimensionPixelSize(height);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusHeight;
    }
}
