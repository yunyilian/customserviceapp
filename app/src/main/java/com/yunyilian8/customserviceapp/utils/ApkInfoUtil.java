package com.yunyilian8.customserviceapp.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApkInfoUtil {

	/**
	 * 获取apk文件的信息
	 * 
	 * @param context
	 * @param archiveFilePath apk文件的路径。如：/sdcard/download/xx.apk
	 */
	public static Map<String, Object> getApkInfo(Context context, String archiveFilePath) {
		final PackageManager pm = context.getPackageManager();// 获取packagemanager
		PackageInfo info = pm.getPackageArchiveInfo(archiveFilePath, PackageManager.GET_ACTIVITIES);
		if (info != null) {
			ApplicationInfo appInfo = info.applicationInfo;
			if (appInfo != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				// String appName = pm.getApplicationLabel(appInfo).toString();
				String[] split = archiveFilePath.split("/");
				String appName = split[split.length - 1].split("_")[0];

				String packageName = appInfo.packageName;
				Drawable icon = pm.getApplicationIcon(appInfo);
				map.put("appName", appName);
				map.put("packageName", packageName);
				map.put("icon", icon);
				map.put("versionCode", info.versionCode);
				return map;
			}
		}
		return null;
	}

	public static String getVersionName(Context context) {
		String version = "";
		try {
			PackageManager packageManager = context.getPackageManager();
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageInfo packInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			version = packInfo.versionName;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return version;
	}

	public static int getVersionCode(Context context) {
		int verCode = -1;
		try {
			verCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return verCode;
	}

	/**
	 * 查询手机内所有支持分享的应用
	 * @param context
	 * @return
	 */
	public static List<ResolveInfo> getShareApps(Context context) {
		List<ResolveInfo> apps = new ArrayList<ResolveInfo>();
		Intent intent = new Intent(Intent.ACTION_SEND, null);
		intent.addCategory(Intent.CATEGORY_DEFAULT);
		intent.setType("text/plain");
		PackageManager pManager = context.getPackageManager();
		apps = pManager.queryIntentActivities(intent, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
		return apps;
	}

	public static int getAndroidSDKVersion() {
		int version = 0;
		try {
			version = Integer.valueOf(android.os.Build.VERSION.SDK);
		} catch (NumberFormatException e) {
		}
		return version;
	}
}
