package com.yunyilian8.customserviceapp.utils;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.util.Property;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;


/**
 * 作者：wyb on 2016/11/3 14:15
 * 邮箱：276698048@qq.com
 * 说明:
 */

public class AnimationUtils {
    private static int DURATION300 = 300;
    private static int DURATION500 = 500;
    private static MyAnimationListener mListener;
    private static ScaleAnimationListener mScaleListener;

    /**
     * 控件宽度 - 从右侧到左侧(双控件)
     *
     * @param leftView
     * @param rightView
     */
    public static void rightToLeft(View leftView, View rightView) {
        final TranslateAnimation mAnimation01;
        final TranslateAnimation mAnimation02;

        /**位移动画**/
        mAnimation01 = new TranslateAnimation(0, -leftView.getWidth(), 0, 0);
        mAnimation02 = new TranslateAnimation(leftView.getWidth(), 0, 0, 0);
        mAnimation01.setDuration(DURATION300);
        mAnimation02.setDuration(DURATION300);
        mAnimation01.setFillAfter(true);
        mAnimation02.setFillAfter(true);
        leftView.startAnimation(mAnimation01);
        rightView.startAnimation(mAnimation02);

        leftView.setVisibility(View.GONE);
        rightView.setVisibility(View.VISIBLE);
        rightView.setClickable(true);
        leftView.setClickable(false);
    }
    /**
     * 控件宽度 - 从右侧到左侧隐藏(单控件)
     *
     * @param view
     */
    public static void rightToLeftConceal(View view) {
        view = getMeasuredView(view);
        final TranslateAnimation mAnimation;
        /**位移动画**/
        mAnimation = new TranslateAnimation(0,-view.getMeasuredWidth() , 0, -view.getMeasuredHeight());
        mAnimation.setDuration(DURATION300);
        mAnimation.setFillAfter(false);
        view.startAnimation(mAnimation);
        view.setVisibility(View.GONE);
        view.setClickable(false);
    }

    /**
     * 控件宽度 - 从右侧到左侧隐藏(单控件)
     *
     * @param view
     */
    public static void rightToLeftConceal02(View view,MyAnimationListener listener) {
        mListener = listener;
        view = getMeasuredView(view);
        final TranslateAnimation mAnimation;
        /**位移动画**/
        mAnimation = new TranslateAnimation(0,-view.getMeasuredWidth()*2 , 0, 0);
        mAnimation.setDuration(DURATION300);
        mAnimation.setFillAfter(false);
        view.startAnimation(mAnimation);
        mAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mListener.isEnd();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    /**
     * 控件宽度 - 从右侧到左侧显示(单控件)
     *
     * @param view
     */
    public static void rightToLeftShow(View view) {
        view = getMeasuredView(view);
        final TranslateAnimation mAnimation;
        /**位移动画**/
        mAnimation = new TranslateAnimation(1000,0 , -view.getMeasuredHeight(),0);
        mAnimation.setDuration(DURATION300);
        mAnimation.setFillAfter(false);
        view.startAnimation(mAnimation);
        view.setVisibility(View.VISIBLE);
        view.setClickable(true);
    }
    /**
     * 控件宽度 - 从左侧到右侧显示(单控件)
     *
     * @param view
     */
    public static void leftToRightShow(View view) {

        view = getMeasuredView(view);

        final TranslateAnimation mAnimation;
        /**位移动画**/
        mAnimation = new TranslateAnimation(-view.getMeasuredWidth(),0, -view.getMeasuredHeight(),0);
        mAnimation.setDuration(DURATION300);
        mAnimation.setFillAfter(false);
        view.startAnimation(mAnimation);
        view.setVisibility(View.VISIBLE);
        view.setClickable(true);

    }
    /**
     * 控件宽度 - 从左侧到右侧隐藏(单控件)
     *
     * @param view
     */
    public static void leftToRightConceal(View view) {

        view = getMeasuredView(view);

        final TranslateAnimation mAnimation;
        /**位移动画**/
        mAnimation = new TranslateAnimation(0,1000, 0, -view.getMeasuredHeight());
        mAnimation.setDuration(DURATION300);
        mAnimation.setFillAfter(false);
        view.startAnimation(mAnimation);
        view.setVisibility(View.GONE);
        view.setClickable(false);

    }

    /**
     * 控件从自己高度到底部隐藏
     *
     * @param view
     */
    public static void heightToBottom(final View view) {
        View newView = getMeasuredView(view);
        /**动画**/
        final ObjectAnimator translationY01 = ObjectAnimator.ofFloat(view, "translationY", 0, newView.getMeasuredHeight());
        /**动画时长**/
        translationY01.setDuration(DURATION500);
        /**开启动画**/
        translationY01.start();

        /**动画监听,在动画结束时候将控件隐藏**/
        translationY01.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        newView.setClickable(false);
    }

    /**
     * 控件从底部弹起到该控件高度显示
     *
     * @param view
     */
    public static void bottomToHeight(View view) {
        view = getMeasuredView(view);
        /**动画**/
        final ObjectAnimator translationY02 = ObjectAnimator.ofFloat(view, "translationY", view.getMeasuredHeight() + 0.5F, 0);
        /**动画时长**/
        translationY02.setDuration(DURATION500);
        /**打开动画**/
        translationY02.start();
        /**控件显示**/
        view.setVisibility(View.VISIBLE);
        view.setClickable(true);
    }

    /**
     * 放大再缩小
     * @param view
     */
    public static void setScaleAnimation(View view )
    {
        view = getMeasuredView(view);

        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleX", 1f,
                2, 1f);
        PropertyValuesHolder pvhZ = PropertyValuesHolder.ofFloat("scaleY", 1f,
                2, 1f);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(view, pvhY, pvhZ).setDuration(500);
        animator.start();
    }

    public interface MyAnimationListener{
        void isEnd();
    }
    public interface ScaleAnimationListener{
        void isEnd();
    }

    /**
     * 测量view
     * @return
     */
    public static View getMeasuredView(View view) {
        int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        view.measure(w, h);
        return view;
    }

    /**
     * An implementation of {@link Property} to be used specifically with fields of
     * type
     * <code>float</code>. This type-specific subclass enables performance benefit by allowing
     * calls to a {@link #set(Object, Float) set()} function that takes the primitive
     * <code>float</code> type and avoids autoboxing and other overhead associated with the
     * <code>Float</code> class.
     *
     * @param <T> The class on which the Property is declared.
     **/
    public static abstract class FloatProperty<T> extends Property<T, Float> {
        public FloatProperty(String name) {
            super(Float.class, name);
        }
    }
}
