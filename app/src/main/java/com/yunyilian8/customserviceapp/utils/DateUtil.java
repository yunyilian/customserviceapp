package com.yunyilian8.customserviceapp.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class DateUtil {

	static Calendar calendar = Calendar.getInstance();

	public static HashMap<String, int[]> getIntArray(String date, boolean isNext) {

		int[] intArr = printWeekdays(date);
		int[] intDataArrat = new int[7];

		HashMap<String, int[]> map = new HashMap<String, int[]>();
		map.put("DateArr", intArr);
		map.put("DateData", intDataArrat);
		return map;
	}
	
	public static String getDateStrByPlusMonth(int plus, String formatStr) {
		SimpleDateFormat format = new SimpleDateFormat(formatStr);
		calendar.add(Calendar.MONTH, plus); // 得到前一个月
		return format.format(calendar.getTime());
	}

	private static final int FIRST_DAY = Calendar.MONDAY;

	private static int[] intArr = new int[7];

	private static int[] printWeekdays(String date) {
		calendar.setTime(formatStrToDate2(date));
		setToFirstDay(calendar);
		for (int i = 0; i < 7; i++) {
			intArr[i] = calendar.get(Calendar.DAY_OF_MONTH);
//			mStrArr[i] = getDateStrByPlusMonth(0, "yyyy-MM-") + (intArr[i] < 10 ? "0" + intArr[i] : intArr[i]);

			calendar.add(Calendar.DATE, 1);
		}
		return intArr;
	}

	private static Date formatStrToDate2(String str) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = sdf.parse(str);
			return date;
		} catch (ParseException e1) {
			e1.printStackTrace();
			return new Date();
		}
	}

	private static void setToFirstDay(Calendar calendar) {
		while (calendar.get(Calendar.DAY_OF_WEEK) != FIRST_DAY) {
			calendar.add(Calendar.DATE, -1);
		}
	}

	/**
	 * 
	 * @Title: getDayOfMonth
	 * @Description: TODO(获取当前月的天数)
	 * @param @return 设定文件
	 * @return int 返回类型
	 * @throws
	 */
	public static int getDayOfMonth(boolean isNext) {
		Calendar aCalendar = Calendar.getInstance(Locale.CHINA);
		if (isNext) {
			aCalendar.add(Calendar.MONTH, 1);
		}
		int day = aCalendar.getActualMaximum(Calendar.DATE);
		return day;
	}

	/**
	 * 格式化时间
	 * 
	 * @param timeMillis
	 * @return yyyy-MM-dd
	 */
	public static String formatDataToYMD(long timeMillis) {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		calendar.setTimeInMillis(timeMillis);
		return formatter.format(calendar.getTime());
	}

	public static int getMonth() {
		return calendar.get(Calendar.MONTH) + 1;
	}

	public static int getDayOfWeek(){
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

}



