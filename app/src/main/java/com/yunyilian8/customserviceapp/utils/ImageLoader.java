package com.yunyilian8.customserviceapp.utils;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.yunyilian8.customserviceapp.R;

/**
 * @author: kermit
 * @date: 2016/8/22
 * Class description:
 * ---- 图片加载库
 */

public class ImageLoader {

    public static void displayImage(Context context, ImageView view, String url) {
        Glide.with(context)
                .load(url)
                .centerCrop()
                .fitCenter()
                .thumbnail(0.1f)
                .crossFade(100)
                .placeholder(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .priority(Priority.HIGH)
                .into(view);
    }
    public static void displayGifImage(Context context, ImageView view, String url) {
        Glide.with(context)
                .load(url)
                .placeholder(R.mipmap.ic_launcher)
                .thumbnail(0.1f)
                .crossFade(100)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .priority(Priority.HIGH)
                .into(view);
    }

    public static void displayDrawableImage(Context context, ImageView view, int drawableId) {
        Glide.with(context)
                .load(drawableId)
                .priority(Priority.HIGH)
                .into(view);
    }

    public static void displaySDImage(Context context, ImageView view, String localFile) {
        Glide.with(context)
                .load("file://" + localFile)
                .centerCrop()
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .priority(Priority.HIGH)
                .into(view);
    }

    public static void displayAssetsImage(Context context, ImageView view, String fileName) {
        Glide.with(context)
                .load("file:///android_asset/" + fileName)
                .centerCrop()
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .priority(Priority.HIGH)
                .into(view);
    }


/*    public static void displayCircleImage(final Context context, ImageView view, String url) {
        Glide.with(context)
                .load(url)
                .asBitmap()
                .error(R.drawable.ka_head_default)
                .placeholder(R.drawable.ka_head_default)
                .into(new BitmapImageViewTarget(view) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        view.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }*/

    public static void clearMemoryCache(Context context) {
        Glide.get(context).clearMemory();
    }

    public static void clearView(View view){
        Glide.clear(view);
    }

}
