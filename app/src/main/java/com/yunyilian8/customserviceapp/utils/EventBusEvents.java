package com.yunyilian8.customserviceapp.utils;


import de.greenrobot.event.EventBus;

/**
 * EventBus通知事件
 * Created by Andy on 2016/4/20.
 */
public class EventBusEvents {

    public static void EventBusChange(AnyEventType e){
        EventBus.getDefault().post(e);
    }

    //开发者-详细信息页面activity
    public static final int EVENT_BROADCAST_DEVELOPER_DETAIL= 10001;

}
