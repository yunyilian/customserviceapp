package com.yunyilian8.customserviceapp.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import java.util.List;

public class ActivityUtil {
	/**
	 * 需添加权限<uses-permission android:name="android.permission.GET_TASKS" />
	 * 
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static boolean isTopActivity(Context context, String packageName) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasksInfo = activityManager.getRunningTasks(1);
		if (tasksInfo.size() > 0) {
			if (packageName.equals(tasksInfo.get(0).topActivity.getPackageName())) {
				return true;
			}
		}
		return false;
	}

	public static boolean isTopActivityName(Context context, String activityName) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		RunningTaskInfo info = manager.getRunningTasks(1).get(0);
		String className = info.topActivity.getClassName(); // 完整类名
		return className.equals(activityName);
	}

	/**
	 * 打开App
	 * 
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static boolean openApp(Context context, String packageName) {
		Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
		if (intent != null) {
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 判断app是否在前台
	 * @param context
	 * @param appPackageName
	 * @return
	 */
	public static boolean isRunningForeground(Context context, String appPackageName) {
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
		String currentPackageName = cn.getPackageName();
		if (!TextUtils.isEmpty(currentPackageName) && currentPackageName.equals(appPackageName)) {
			return true;
		}
		return false;
	}

	/**
	 * 需添加权限<uses-permission
	 * android:name="android.permission.KILL_BACKGROUND_PROCESSES" />
	 * 
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static boolean closeApp(Context context, String packageName) {
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		am.killBackgroundProcesses(packageName);
		return true;
	}

	/**
	 * 获取堆顶ActivityName
	 * 
	 * @param context
	 * @return
	 */
	public static String getTopActivityName(Context context) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasksInfo = activityManager.getRunningTasks(1);
		if (tasksInfo.size() > 0) {
			return tasksInfo.get(0).topActivity.getPackageName();
		}
		return "";
	}
}
