package com.yunyilian8.customserviceapp.utils;

import android.os.Environment;

/**
 * 作者：wyb on 2016/10/14 11:23
 * 邮箱：276698048@qq.com
 * 说明:
 */

public class SDCardUtils {
    /*
       * 判断sdcard是否被挂载
       */
    public static boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }
}
