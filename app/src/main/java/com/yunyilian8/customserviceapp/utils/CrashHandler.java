package com.yunyilian8.customserviceapp.utils;

import android.content.Context;

import java.lang.Thread.UncaughtExceptionHandler;


public class CrashHandler implements UncaughtExceptionHandler {

	private UncaughtExceptionHandler defaultExceptionHandler;
	// 单例声明CustomException;
	private static CrashHandler customException;

	private CrashHandler() {
	}

	public static CrashHandler getInstance() {
		if (customException == null) {
			customException = new CrashHandler();
		}
		return customException;
	}

	@Override
	public void uncaughtException(Thread thread, Throwable exception) {
		// TODO Auto-generated method stub
		if (defaultExceptionHandler != null) {
			defaultExceptionHandler.uncaughtException(thread, exception);
		}
	}

	public void init(Context context) {
		defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(this);

	}
}
