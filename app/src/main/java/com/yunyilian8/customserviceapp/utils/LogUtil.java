package com.yunyilian8.customserviceapp.utils;

import android.content.Context;
import android.util.Log;

public class LogUtil {

    //决定是否输出日志
    //  private static boolean DEBUG = true;

     private static boolean DEBUG = false;

    public static void setDebug(boolean debug) {
        DEBUG = debug;
    }

    public static void LogD(Context con, String msg) {
        if (DEBUG) {
            Log.d(con.getClass().getSimpleName(), msg);
        }
    }

    public static void LogI(Context con, String msg) {
        if (DEBUG) {
            Log.i(con.getClass().getSimpleName(), msg);
        }
    }

    public static void LogE(Context con, String msg) {
        if (DEBUG) {
            Log.e(con.getClass().getSimpleName(), msg);
        }
    }

    public static boolean isDebug() {
        return DEBUG;
    }

    public static void v(String tag, String message) {
        if (DEBUG) {
            Log.v(tag, message);
        }
    }

    public static void d(String tag, String message) {
        if (DEBUG) {
            Log.d(tag, message);
        }
    }
    public static void logD(String tag, String content) {
        int p = 2000;
        long length = content.length();
        if (length < p || length == p)
            Log.e(tag, content);
        else {
            while (content.length() > p) {
                String logContent = content.substring(0, p);
                content = content.replace(logContent, "");
                Log.e(tag, logContent);
            }
            Log.e(tag, content);
        }
    }

    public static void i(String tag, String message) {
        if (DEBUG) {
            Log.i(tag, message);
        }
    }

    public static void w(String tag, String message) {
        if (DEBUG) {
            Log.w(tag, message);
        }
    }

    public static void e(String tag, String message) {
        if (DEBUG) {
            Log.e(tag, message);
        }
    }
}
