package com.yunyilian8.customserviceapp.utils;

import android.content.Context;
import android.text.TextUtils;

/**
 * Created by Administrator on 2016/4/8.
 */
public class BeanUtil {

    public static int updateInt(int a, int b) {
        if (a != b)
            a = b;
        return a;
    }

    public static float updateFloat(float a, float b) {
        if (a != b)
            a = b;
        return a;
    }

    public static String updateString(String a, String b) {
        if (TextUtils.isEmpty(a)) {
            a = b;
        } else if (!TextUtils.isEmpty(b) && !a.equals(b)) {
            a = b;
        }
        return a;
    }

    public static String getAccessToken(Context mCtx) {
        return SharePreUtil.getString(mCtx, "TokenId", "");
    }
}
