package com.yunyilian8.customserviceapp.utils;

public class AnyEventType {
	public int mType;
	public Object mObj;

	public AnyEventType(int type,Object obj) {
		mType = type;
		mObj = obj;
	}
	public int getType(){
		return mType;
	}
	public Object getObj(){
		return mObj;
	}
}
