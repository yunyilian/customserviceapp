package com.yunyilian8.customserviceapp.utils;

import android.os.Environment;

/**
 * Created by Administrator on 2016/5/3.
 */
public class Contants {


    public static final String RootPath = getRootPath() + "/serviceAPP/";

    private static String getRootPath() {
        return Environment.getExternalStorageDirectory().getPath();
    }

    public static final String ImgCachePath = RootPath + "user_pic/";
    public static final String PhotoSavePath = RootPath + "pic/";
    public static int sYear, sMonth, sDay; // 公历年月日

    public static final String UserInfo = RootPath + "ser/User_info.ini";
    public static final String DownloadPath = RootPath + "download/";

    /**app崩溃log收集的文件夹地址**/
    public static final String ExceptionLogPath = RootPath + "exception/";

}
