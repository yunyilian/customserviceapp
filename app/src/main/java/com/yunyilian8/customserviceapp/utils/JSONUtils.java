package com.yunyilian8.customserviceapp.utils;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;


public class JSONUtils {

    private static Gson mGson;

    public static Gson getGson() {
        if (null == mGson) {
            mGson = new Gson();
        }
        return mGson;
    }

    /**
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> parseJson2List(String json,Class<T> clazz){

        List<T> list=null;

        list=JSON.parseArray(json, clazz);

        return list;

    }

    /**
     * @param json
     * @param clazz
     * @return
     * @return T
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    public static <T> T toObjectByJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {

        return JSON.parseObject(json, clazz)/* om.readValue(json, clazz) */;

    }


    /**
     * 将json字符串转换成一个对象
     *
     * @param jsonStr json字符串
     * @param field   字段名
     * @param clazz   字节码
     * @param <T>     类型
     * @return
     */
    public static <T> T getObject(String jsonStr, String field, Class<T> clazz) {
        JSONObject jsonObject = getJSONObject(jsonStr);
        T t = getGson().fromJson(jsonObject.optString(field), clazz);

        return t;
    }

    /**
     * 将json字符串转换成list数据
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> getList(String json, Class<T> clazz) {

        List<T> list = null;

        list = JSON.parseArray(json, clazz);

        return list;
    }

    /**
     * 将json字符串转换成jsonObject
     *
     * @param jsonStr
     * @return
     */
    public static JSONObject getJSONObject(String jsonStr) {
        JSONObject mJson = new JSONObject();
        try {
            mJson = new JSONObject(jsonStr);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mJson;
    }

    /**
     * 将jsonObect转换成jsonArray
     *
     * @param jsonObj
     * @param name
     * @return
     */
    public static JSONArray getJSONArray(JSONObject jsonObj, String name) {
        JSONArray mArray = new JSONArray();
        try {
            mArray = jsonObj.getJSONArray(name);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mArray;
    }

    /**
     * 通过jsonObject获取字符串值
     *
     * @param jsonStr json字符串
     * @param name
     * @return
     */
    public static String getValue(String jsonStr, String name) {
        try {
            JSONObject jsonObject = getJSONObject(jsonStr);
            if (!jsonObject.isNull(name))
                return jsonObject.getString(name);
            else
                return "";
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 通过jsonObject获取int值
     *
     * @param mJson
     * @param name
     * @return
     */
    public synchronized static int getIntValue(JSONObject mJson, String name) {
        try {
            if (!mJson.isNull(name))
                return Integer.parseInt(mJson.getString(name));
            else
                return -1;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return -1;
        }
    }

}