//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yunyilian8.customserviceapp.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;


/**
 * 邮箱：276698048@qq.com
 * 说明: 通过监听滑动的控件位置以及回调接口，将scrollView的滑动数据传递给调用者。
 */
public class MyScrollView02 extends ScrollView {

    private static boolean HEAD_OFF = true;
    private static boolean TOUCH_STOP = false;
    private static boolean IS_BOTTOM = false;
    protected int mNextX;
    private Context mContext;
    private float startx;
    private float starty;
    private LoadDateListener mListener;
    private int mTouchY;

    public MyScrollView02(Context context) {
        super(context);
        this.mContext = context;
    }

    public MyScrollView02(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyScrollView02(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyScrollView02(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected int computeScrollDeltaToGetChildRectOnScreen(Rect rect) {
        return 0;
    }


    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        mListener.scroll(t);
        View view = (View) getChildAt(getChildCount() - 1);
        int d = view.getBottom();
        d -= (getHeight() + getScrollY());
        if (d == 0) {
            if (null != mListener)
                mListener.isBottom();
            IS_BOTTOM = true;
        } else if (d > 0) {
            if (null != mListener)
                mListener.notBottom();
            IS_BOTTOM = false;
        } else
            super.onScrollChanged(l, t, oldl, oldt);
    }

    public void setLoadDateListener(LoadDateListener listener) {
        this.mListener = listener;
    }


    public interface LoadDateListener {
        void isBottom();

        void notBottom();

        void scroll(int roll);

        void isTouchStop();
    }
}