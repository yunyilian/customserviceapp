package com.yunyilian8.customserviceapp.view.flowtaglayout;

import android.view.View;

/**
 * Created by HanHailong on 15/10/20.
 */
public interface OnTagClickListener02 {
    void onItemClick(FlowTagLayout02 parent, View view, int position);
}
