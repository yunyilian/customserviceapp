package com.yunyilian8.customserviceapp.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import com.yunyilian8.customserviceapp.utils.UITools;

/**
 * 在鸿洋写的该类的基础上进行了修改(达到自己要的效果:四周都有分割线)
 * @author zhy
 *
 */
public class DividerGridItemDecoration extends RecyclerView.ItemDecoration
{

    private static final int[] ATTRS = new int[] { android.R.attr.listDivider };
    private Drawable mDivider;
    /** 显示的列数 */
    private int column_num;
    /**左右的距离**/
    private int LeftAndRightNum = 0;
    /**上下的距离**/
    private int TopAndBottomNum = 0;

    /**
     *
     * @param context
     * @param num  显示的分割线数
     * @param leftAndRight  需要设置左右宽度的dp值
     * @param TopAndBottom  需要设置上下宽度的dp值
     */
    public DividerGridItemDecoration(Context context, int num, int leftAndRight,int TopAndBottom)
    {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
        column_num = num;
        this.LeftAndRightNum = UITools.dip2px(context,leftAndRight);
        this.TopAndBottomNum = UITools.dip2px(context,TopAndBottom);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state)
    {

        drawHorizontal(c, parent);
        drawVertical(c, parent);

    }

    private int getSpanCount(RecyclerView parent)
    {
        // 列数
        int spanCount = -1;
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager)
        {

            spanCount = ((GridLayoutManager) layoutManager).getSpanCount();
        } else if (layoutManager instanceof StaggeredGridLayoutManager)
        {
            spanCount = ((StaggeredGridLayoutManager) layoutManager)
                    .getSpanCount();
        }
        return spanCount;
    }

    public void drawHorizontal(Canvas c, RecyclerView parent)
    {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++)
        {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int left = child.getLeft() - params.leftMargin;
            final int right = child.getRight() + params.rightMargin
                    + TopAndBottomNum;
            int top = 0;
            int bottom = 0;

            if((i/column_num) == 0) {
                //画item最上面的分割线
                top = 0;
                bottom = top + TopAndBottomNum;
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
                mDivider.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.MULTIPLY);
                //画item下面的分割线
                top = child.getBottom() + params.bottomMargin;
                bottom = top + TopAndBottomNum;
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
                mDivider.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.MULTIPLY);
            }
            else {
                //画item下面的分割线
                top = child.getBottom() + params.bottomMargin;
                bottom = top + TopAndBottomNum;
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
                mDivider.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.MULTIPLY);
            }

        }
    }

    public void drawVertical(Canvas c, RecyclerView parent)
    {
        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++)
        {
            final View child = parent.getChildAt(i);

            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getTop() - params.topMargin;
            final int bottom = child.getBottom() + params.bottomMargin;
            int left = 0;
            int right = 0;

            if((i%column_num) == 0) {
                //item左边分割线
                left = 0;
                right = left + LeftAndRightNum;
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
                mDivider.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.MULTIPLY);
                //item右边分割线
                left = child.getRight() + params.rightMargin;
                right = left + LeftAndRightNum;
            }
            else {
                left = child.getRight() + params.rightMargin;
                right = left + LeftAndRightNum;
            }

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
            mDivider.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.MULTIPLY);
        }
    }

    private boolean isLastColum(RecyclerView parent, int pos, int spanCount,
                                int childCount)
    {
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager)
        {
/*            if ((pos + 1) % spanCount == 0)// 如果是最后一列，则不需要绘制右边
            {
                return true;
            }*/
        } else if (layoutManager instanceof StaggeredGridLayoutManager)
        {
            int orientation = ((StaggeredGridLayoutManager) layoutManager)
                    .getOrientation();
            if (orientation == StaggeredGridLayoutManager.VERTICAL)
            {
                if ((pos + 1) % spanCount == 0)// 如果是最后一列，则不需要绘制右边
                {
                    return true;
                }
            } else
            {
                childCount = childCount - childCount % spanCount;
                if (pos >= childCount)// 如果是最后一列，则不需要绘制右边
                    return true;
            }
        }
        return false;
    }

    private boolean isLastRaw(RecyclerView parent, int pos, int spanCount,
                              int childCount)
    {
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager)
        {
/*             childCount = childCount - childCount % spanCount;
           if (pos >= childCount)// 如果是最后一行，则不需要绘制底部
                return true;*/
        } else if (layoutManager instanceof StaggeredGridLayoutManager)
        {
            int orientation = ((StaggeredGridLayoutManager) layoutManager)
                    .getOrientation();
            // StaggeredGridLayoutManager 且纵向滚动
            if (orientation == StaggeredGridLayoutManager.VERTICAL)
            {
                childCount = childCount - childCount % spanCount;
                // 如果是最后一行，则不需要绘制底部
                if (pos >= childCount)
                    return true;
            } else
            // StaggeredGridLayoutManager 且横向滚动
            {
                // 如果是最后一行，则不需要绘制底部
                if ((pos + 1) % spanCount == 0)
                {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void getItemOffsets(Rect outRect, int itemPosition,
                               RecyclerView parent)
    {
        int spanCount = getSpanCount(parent);
        int childCount = parent.getAdapter().getItemCount();
        if (isLastRaw(parent, itemPosition, spanCount, childCount))// 如果是最后一行，则不需要绘制底部
        {
            outRect.set(0, 0, LeftAndRightNum, 0);
        } else if (isLastColum(parent, itemPosition, spanCount, childCount))// 如果是最后一列，则不需要绘制右边
        {
            outRect.set(0, 0, 0, TopAndBottomNum);
        } else
        {
            outRect.set(LeftAndRightNum, TopAndBottomNum, LeftAndRightNum,
                    TopAndBottomNum);
        }
    }
}
