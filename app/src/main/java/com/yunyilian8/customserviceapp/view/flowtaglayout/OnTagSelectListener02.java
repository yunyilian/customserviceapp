package com.yunyilian8.customserviceapp.view.flowtaglayout;

import java.util.List;

/**
 * Created by HanHailong on 15/10/20.
 */
public interface OnTagSelectListener02 {
    void onItemSelect(FlowTagLayout02 parent, List<Integer> selectedList,int position);
}
