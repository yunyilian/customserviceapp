package com.yunyilian8.customserviceapp.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.utils.UITools;

/**
 * 作者：wyb on 2016/10/13 14:21
 * 邮箱：276698048@qq.com
 * 说明: 设置Info的布局抽取自定义
 *      其中mLayoutType代表布局的类型,为int值:
 *      1:无左侧图片,但左侧右侧都有文字
 *      2:无左侧图片,且只有左侧有文字
 *      3:有左侧图片,只有左侧有文字
 *      4:有左侧图片,且左侧右侧有文字
 */

public class SettingItemLayout extends RelativeLayout {

    private int itmeTypy;
    private String mTitleText = "";
    private int mTitleTextColor = R.color.black;
    private String mContentText = "";
    private int mContentTextColor = R.color.gray10;
    private int mBackColor = R.color.white;
    private int mIconImage = R.mipmap.icon_arrow_right;
    private RelativeLayout mRlLayout;
    private int mLayoutType;
    private TextView mTvContent;
    private int mStyleImage= R.mipmap.icon_arrow_right;
    private int mItemBackgroundResource = R.drawable.selector_bg_gray4_gray6;
    private final int PADDING_LEFT = 15;
    private final int PADDING = 0;
    private final int LAYOUT_TYPE_A = 1;
    private final int LAYOUT_TYPE_B = 2;
    private final int LAYOUT_TYPE_C = 3;
    private final int LAYOUT_TYPE_D = 4;

    public SettingItemLayout(Context context) {
        super(context);
        initType(context, null, 0, 0);
        initView();
    }

    public SettingItemLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initType(context, attrs, 0, 0);
        initView();
    }

    public SettingItemLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initType(context, attrs, defStyleAttr, 0);
        initView();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SettingItemLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initType(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    protected void initType(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SettingItemLayout, defStyleAttr, defStyleRes);

        int n = a.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.SettingItemLayout_titleText:
                    mTitleText = a.getString(attr);
                    break;
                case R.styleable.SettingItemLayout_mtitleTextColor:
                    mTitleTextColor = a.getResourceId(attr, R.color.color_F1F4F4);
                    break;
                case R.styleable.SettingItemLayout_contentText:
                    mContentText = a.getString(attr);
                    break;
                case R.styleable.SettingItemLayout_contentTextColor:
                    mContentTextColor = a.getResourceId(attr, R.color.color_F1F4F4);
                    break;
                case R.styleable.SettingItemLayout_styleImage:
                    mStyleImage = a.getResourceId(attr, mStyleImage);
                    break;
                case R.styleable.SettingItemLayout_iconImage:
                    mIconImage = a.getResourceId(attr, mIconImage);
                    break;
                case R.styleable.SettingItemLayout_layoutType:
                    mLayoutType = a.getInt(attr, 1);
                    break;
                case R.styleable.SettingItemLayout_itemBackgroundResource:
                    mItemBackgroundResource = a.getResourceId(attr,R.drawable.selector_bg_gray4_gray6);
                    break;
            }
        }
        a.recycle();
    }

    protected void initView() {
        /**填充布局**/
        View layout = View.inflate(getContext(), R.layout.layout_setting_info, null);
        mRlLayout = (RelativeLayout) layout.findViewById(R.id.rl_layout);
        TextView tvTitle = (TextView) layout.findViewById(R.id.tv_title);
        mTvContent = (TextView) layout.findViewById(R.id.tv_content);
        ImageView ivIamge = (ImageView) layout.findViewById(R.id.iv_image);
        ImageView ivStyleImage = (ImageView) layout.findViewById(R.id.iv_style_image);
        /**设置**/
        tvTitle.setTextColor(getContext().getResources().getColor(mTitleTextColor));
        tvTitle.setText(mTitleText);
        mTvContent.setTextColor(getContext().getResources().getColor(mContentTextColor));
        mTvContent.setText(mContentText);
        ivIamge.setImageDrawable(getContext().getResources().getDrawable(mIconImage));
        ivStyleImage.setImageDrawable(getResources().getDrawable(mStyleImage));
 //       setBackgroundResource(mBackSelector);
        mRlLayout.setBackgroundResource(mItemBackgroundResource);

        if (mLayoutType == LAYOUT_TYPE_B)
        {
            /**类型2,左侧有文字,右侧无文字**/
            mTvContent.setVisibility(View.GONE);
        }else if (mLayoutType == LAYOUT_TYPE_C)
        {
            /**类型3,左侧有图片,左侧有文字,右侧无文字**/
            ivStyleImage.setVisibility(View.VISIBLE);
            tvTitle.setPadding(UITools.dip2px(getContext(),PADDING_LEFT),PADDING,PADDING,PADDING);
            mTvContent.setVisibility(View.GONE);
        }else if (mLayoutType == LAYOUT_TYPE_D)
        {
            /**类型4,左侧有图片,左侧右侧有文字**/
            ivStyleImage.setVisibility(View.VISIBLE);
            tvTitle.setPadding(UITools.dip2px(getContext(),PADDING_LEFT),PADDING,PADDING,PADDING);
        }

        addView(layout);
    }

    public void setContentText(String contentText)
    {
        mTvContent.setText(contentText);
    }
    public String getContentText(){
        return mTvContent.getText().toString().trim();
    }

}
