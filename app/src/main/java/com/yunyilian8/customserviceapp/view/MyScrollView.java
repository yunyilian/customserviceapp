//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yunyilian8.customserviceapp.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;


/**
 * 邮箱：276698048@qq.com
 * 说明: 1.重新ScrollView的computeScrollDeltaToGetChildRectOnScreen方法,解决scrollView自动滚动到底部问题
 *      2.增加ScrollView滑动到底部的判断,并且增加监听其滑动幅度,滑动到底部以及手势结束的监听
 */
public class MyScrollView extends ScrollView {

    private static boolean TOUCH_STOP = false;
    private static boolean IS_BOTTOM = false;
    protected int mNextX;
    private Context mContext;
    private float startx;
    private float starty;
    private LoadDateListener mListener;
    private int mTouchY;

    public MyScrollView(Context context) {
        super(context);
        this.mContext = context;
    }

    public MyScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    @Override
    protected int computeScrollDeltaToGetChildRectOnScreen(Rect rect) {    return 0; }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean result = super.onTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startx = event.getX();
                starty = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                int touchX = (int) (event.getX() - startx);
                mTouchY = (int)(event.getY() - starty);
                Log.d("onScrollChanged","(01最外层。。。)"+ starty +":" + mTouchY);
                if (IS_BOTTOM)
                {
                    /**判断用户是垂直向上滚动**/
                    if (Math.abs(mTouchY) > Math.abs(touchX))
                    {
                        /**判断用户是向上滚动**/
                        if(mTouchY < 0)
                        {
                            if (null != mListener )
                                mListener.scroll(Math.abs(mTouchY));
                        }
                    }
                }
                TOUCH_STOP = false;

                invalidate();
            case MotionEvent.ACTION_UP:
                if (TOUCH_STOP)
                {
                    if (null != mListener )
                    {
                        /**判断用户是向上滚动**/
                        if(mTouchY < 0)
                        {
                            mListener.isTouchStop();
                        }
                    }
                }
                TOUCH_STOP = true;
            default:
                break;
        }
        return result;
    }


    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt)
    {
        View view = (View)getChildAt(getChildCount()-1);
        int d = view.getBottom();
        d -= (getHeight()+getScrollY());
        if(d==0)
        {
            if (null != mListener )
                mListener.isBottom();
            IS_BOTTOM = true;
        }else if (d > 0){
            if (null != mListener )
                mListener.notBottom();
            IS_BOTTOM = false;
        }
        else
            super.onScrollChanged(l,t,oldl,oldt);
    }
    public void setLoadDateListener(LoadDateListener listener)
    {
        this.mListener = listener;
    }
    public interface LoadDateListener
    {
        void isBottom();
        void notBottom();
        void scroll(int roll);
        void isTouchStop();
    }
}