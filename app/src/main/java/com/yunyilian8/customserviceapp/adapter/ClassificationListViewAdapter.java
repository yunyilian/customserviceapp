package com.yunyilian8.customserviceapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.SimpleBaseAdapter;
import com.yunyilian8.customserviceapp.bean.DevelopmentEntryDetailClassificationBean;

import java.util.List;

/**
 * Created by whss on 2016/12/19.
 */

public class ClassificationListViewAdapter extends SimpleBaseAdapter<DevelopmentEntryDetailClassificationBean> {

    public ClassificationListViewAdapter(Context context, List<DevelopmentEntryDetailClassificationBean> list) {
        super(context, list);
        mContext=context;
        mList=list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //ViewHolder
        ViewHolder viewHolder=null;
        //绘制每一项的时候先判断convertView是否为空，不为空，则else里面去复用，为空，则重新赋予item布局
        if (convertView == null) {
            //new出ViewHolder ，初始化布局文件
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_development_enty_detail_classification, null);
            viewHolder.iv_logo = (ImageView) convertView.findViewById(R.id.iv_logo);
            viewHolder.tv_title=(TextView) convertView.findViewById(R.id.tv_personnel);
            viewHolder.tv_describe=(TextView) convertView.findViewById(R.id.tv_tool_language);

            //调用convertView的setTag方法，将viewHolder放入进去，用于下次复用
            convertView.setTag(viewHolder);
        } else {
            //复用已经存在的item的项
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.iv_logo.setImageResource(R.mipmap.ic_launcher);

        return convertView;
    }

    //为了优化ListView不可避免的使用了ViewHolder来复用
    class ViewHolder {

        ImageView iv_logo;
        TextView tv_title;
        TextView tv_describe;
    }
}

