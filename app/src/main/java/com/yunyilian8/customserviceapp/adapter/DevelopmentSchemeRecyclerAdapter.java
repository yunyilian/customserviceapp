package com.yunyilian8.customserviceapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseRecycleViewAdapter;
import com.yunyilian8.customserviceapp.bean.DevelopmentSchemeRecyclerBean;
import com.yunyilian8.customserviceapp.utils.ImageLoader;

import java.util.List;


/**
 * Created by Administrator on 2016/12/15.
 */

public class DevelopmentSchemeRecyclerAdapter extends BaseRecycleViewAdapter<DevelopmentSchemeRecyclerAdapter.MyViewHolder> {
    public DevelopmentSchemeRecyclerAdapter(List<DevelopmentSchemeRecyclerBean> data, Context context) {
        mList = data;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder getHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.item_development_scheme_recycler,parent,false);
        MyViewHolder viewHolder=new MyViewHolder(view);
        return viewHolder;
    }


    @Override
    public void setViewHolder(final MyViewHolder holder, int position) {
        DevelopmentSchemeRecyclerBean bean = (DevelopmentSchemeRecyclerBean)((List) mList).get(position);
        ImageLoader.displayImage(mContext,holder.mIvTitle,bean.getURL());
        holder.mTvName.setText(bean.getName());
        holder.mTvKnow.setText(bean.getKnow());
        holder.mTvPrice.setText(bean.getPrice());
    }


    /**
     * ViewHolder类，这个类的作用主要用于实例化控件
     */
    class MyViewHolder extends RecyclerView.ViewHolder {
        //声明控件
        ImageView mIvTitle;
        TextView mTvName;
        TextView mTvKnow;
        TextView mTvPrice;

        public MyViewHolder(View itemView) {
            super(itemView);
            /**初始化控件**/

            mIvTitle = (ImageView) itemView.findViewById(R.id.iv_title);
            mTvName = (TextView) itemView.findViewById(R.id.tv_name);
            mTvKnow = (TextView) itemView.findViewById(R.id.tv_know);
            mTvPrice = (TextView) itemView.findViewById(R.id.tv_price);
        }
    }
}
