package com.yunyilian8.customserviceapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.BaseRecycleViewAdapter;
import com.yunyilian8.customserviceapp.utils.ImageLoader;

import java.util.List;

/**
 * 作者：wyb on 2016/9/29 15:45
 * 邮箱：276698048@qq.com
 * 说明:首页-分类-recycleView对应adapter
 */

public class MyRecyclerviewAdapter extends BaseRecycleViewAdapter<MyRecyclerviewAdapter.MyViewHolder> {

    public MyRecyclerviewAdapter(List<Integer> data, Context context) {
        mList = data;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder getHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.item_classify_recycler,parent,false);
        MyViewHolder viewHolder=new MyViewHolder(view);
        return viewHolder;
    }


    @Override
    public void setViewHolder(final MyViewHolder holder, int position) {
        ImageLoader.displayDrawableImage(mContext,holder.iv,(int)((List) mList).get(position));
    }


    /**
     * ViewHolder类，这个类的作用主要用于实例化控件
     */
    class MyViewHolder extends RecyclerView.ViewHolder {
        //声明控件
        ImageView iv;

        public MyViewHolder(View itemView) {
            super(itemView);
            /**初始化控件**/

            iv = (ImageView) itemView.findViewById(R.id.iv);
        }
    }
}
