package com.yunyilian8.customserviceapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.view.flowtaglayout.OnInitSelectedPosition;

import java.util.ArrayList;
import java.util.List;

/**
 * 设置可变长度标签对应adapter
 * @param <T>
 */
public class TagAdapter<T> extends BaseAdapter implements OnInitSelectedPosition {

    private final Context mContext;
    private final List<T> mDataList;
    private List<Integer> mOOSList;

    public static int LIST_TYPE_SERVICE = 1;
    public static int LIST_TYPE_INDEMNIFY = 2;
    private int mType;

    public TagAdapter(Context context,int type) {
        this.mContext = context;
        this.mType = type;
        mDataList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {

        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        /*if (mOOSList.contains(position))
        {
            *//**设置不能选中的操作,可以通过遍历一个不选中的数组来实现下....**//*
            return -1;
        }*/
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.tag_item, null);
        TextView textView = (TextView) view.findViewById(R.id.tv_tag);
        T t = mDataList.get(position);

        textView.setText((String) t);


        /*if (t instanceof String) {
            if (mOOSList.contains(position))
            {
                textView.setText((String) t+"(缺货)");
                textView.setBackgroundResource(R.drawable.selector_round_rectangle_bg_oos);
                textView.setTextColor(mContext.getResources().getColor(R.color.gray4));
            }else {

            }
        }*/
        return view;
    }


    public void onlyAddAll(List<T> datas) {
        mDataList.addAll(datas);
        notifyDataSetChanged();
    }
    public void setOOSList(List<Integer> list)
    {
        this.mOOSList = list;
    }

    public void clearAndAddAll(List<T> datas) {
        mDataList.clear();
        onlyAddAll(datas);
    }

    @Override
    public boolean isSelectedPosition(int position) {
        if (position % 2 == 0) {
            return true;
        }
        return false;
    }
}
