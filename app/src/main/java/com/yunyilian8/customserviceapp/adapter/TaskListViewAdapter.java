package com.yunyilian8.customserviceapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.base.SimpleBaseAdapter;
import com.yunyilian8.customserviceapp.bean.AnsweredTaskBean;
import com.yunyilian8.customserviceapp.utils.ImageLoader;
import com.yunyilian8.customserviceapp.utils.ToastUtil;

import java.util.List;

/**
 * 推过来的任务，我发布的，我接的的listView  共同使用的adapter，具体额外设置
 */

public class TaskListViewAdapter extends SimpleBaseAdapter<AnsweredTaskBean> {

    private int mType;

    public TaskListViewAdapter(Context context) {
        super(context);
    }

    public TaskListViewAdapter(Context context, List list, int type) {
        super(context, list);
        this.mType = type;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyHolder holder = null;

        if (null == convertView) {
            holder = new MyHolder();
            /**共同使用同一个布局**/
            convertView = View.inflate(mContext, R.layout.item_task02, null);
            /**初始化view**/
            initViews(holder,convertView);


            convertView.setTag(holder);
        } else {
            holder = (MyHolder) convertView.getTag();
        }

        /**获取对象**/
        AnsweredTaskBean bean = mList.get(position);
        /**设置item中每个布局的监听监听**/
        setListener(holder,convertView,position);
        /**设置显示**/
        visibilityView(bean,holder);
        /**设置数据**/
        setData(bean,holder);

        return convertView;
    }

    /**
     * 设置点击监听
     * @param holder
     * @param convertView
     */
    private void setListener(MyHolder holder, View convertView, final int position) {
        holder.mLlHeart.setOnClickListener(new MyOnClickListener(position,1));
        holder.mLlDustbin.setOnClickListener(new MyOnClickListener(position,2));
        holder.mLlComment.setOnClickListener(new MyOnClickListener(position,3));
    }

    /**
     * 设置数据
     * @param bean
     * @param holder
     */
    private void setData(AnsweredTaskBean bean, MyHolder holder) {
        ImageLoader.displayImage(mContext,holder.mIvHead,bean.getImageURL());
        holder.mTvName.setText(bean.getName());
        holder.mTvContent.setText(bean.getContent());

        switch (mType) {
            case 1:

                break;
            case 2:
                holder.mTvHeart.setText(bean.getPraiseNum());
                holder.mTvComment.setText(bean.getCommentNum());
                break;
            case 3:
                holder.mTvProgress.setText("距离交付任务还有" + bean.getDayNum() + "天");
                holder.mPbProgress.setProgress(bean.getProgress());
                break;
        }
    }

    /**
     * 初始化view
     * @param holder
     * @param convertView
     */
    private void initViews(MyHolder holder, View convertView) {
        holder.mIvHead = (ImageView) convertView.findViewById(R.id.iv_head);
        holder.mTvName = (TextView) convertView.findViewById(R.id.tv_name);
        holder.mTvContent = (TextView) convertView.findViewById(R.id.tv_content);

        holder.mLlDetails = (LinearLayout) convertView.findViewById(R.id.ll_details);
        holder.mLlIssuance = (LinearLayout) convertView.findViewById(R.id.ll_issuance);
        holder.mLlHeart = (LinearLayout) convertView.findViewById(R.id.ll_heart);
        holder.mTvHeart = (TextView) convertView.findViewById(R.id.tv_heart);
        holder.mLlDustbin = (LinearLayout) convertView.findViewById(R.id.ll_dustbin);
        holder.mLlComment = (LinearLayout) convertView.findViewById(R.id.ll_comment);
        holder.mTvComment = (TextView) convertView.findViewById(R.id.tv_comment);

        holder.mLlProgress = (LinearLayout) convertView.findViewById(R.id.ll_progress);
        holder.mTvProgress = (TextView) convertView.findViewById(R.id.tv_progress);
        holder.mPbProgress = (ProgressBar) convertView.findViewById(R.id.pb_progress);
    }

    private void visibilityView(AnsweredTaskBean bean,MyHolder holder) {
        switch (mType) {
            case 1:
                holder.mLlIssuance.setVisibility(View.GONE);
                holder.mLlProgress.setVisibility(View.GONE);
                break;
            case 2:
                holder.mLlDetails.setVisibility(View.GONE);
                holder.mLlProgress.setVisibility(View.GONE);
                break;
            case 3:
                holder.mLlDetails.setVisibility(View.GONE);
                holder.mLlIssuance.setVisibility(View.GONE);
                break;
        }
    }

    class MyHolder {
        /**头图片**/
        ImageView mIvHead;
        /**名字**/
        TextView mTvName;
        /**内容**/
        TextView mTvContent;


        /**详情布局**/
        LinearLayout mLlDetails;


        /**我发布的布局**/
        LinearLayout mLlIssuance;
        /**点赞**/
        LinearLayout mLlHeart;
        /**点赞**/
        TextView mTvHeart;
        /**删除**/
        LinearLayout mLlDustbin;
        /**评论**/
        LinearLayout mLlComment;
        /**评论**/
        TextView mTvComment;


        /**进度布局**/
        LinearLayout mLlProgress;
        /**剩余天数**/
        TextView mTvProgress;
        /**进度条**/
        ProgressBar mPbProgress;
    }

    class MyOnClickListener implements View.OnClickListener {

        private int position;
        private int type;

        public MyOnClickListener(int pos,int layoutType) {
            position = pos;
            type = layoutType;
        }
        @Override
        public void onClick(View v) {
            switch (type) {
                case 1:
                    ToastUtil.showToast(mContext,"type:" + mType + "position:" + position + "点赞");
                    break;
                case 2:
                    ToastUtil.showToast(mContext,"type:" + mType + "position:" + position + "删除");
                    break;
                case 3:
                    ToastUtil.showToast(mContext,"type:" + mType + "position:" + position + "评论");
                    break;
            }
        }
    }
}
