package com.yunyilian8.customserviceapp.service;
/**
 *
 */

import com.yunyilian8.customserviceapp.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Observable;


public class RetrofitService {

    public static final int PAGE_SIZE = 10;

    // 需求/发布 测试
    public final static String ServerRootPath = "http://120.77.168.89/index.php/";

    public final static String passwordMD5 = "whssUCenter";

    private static RetrofitService retrofitService = new RetrofitService();
    private static RetrofitInterface retrofitInterface;

    private RetrofitService() {
        initRetrofit();
    }

    public static RetrofitService getInstance() {
        if (retrofitService == null) {
            retrofitService = new RetrofitService();
        }
        return retrofitService;
    }

    private void initRetrofit() {
        OkHttpClient httpClient;
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .addInterceptor(logging).build();
        } else {
            httpClient = new OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .build();
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ServerRootPath)
                .client(httpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
    }

    public static void changeServerURL(String url) {
        OkHttpClient httpClient;
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .addInterceptor(logging).build();
        } else {
            httpClient = new OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .build();
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(httpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
    }


    /**
     * 获取首页轮播图列表
     */
    public Observable<String> callBannerList() {
        return retrofitInterface.callBannerList();
    }

    /**
     * 注册
     */
    public Observable<String> userRegister(String username, String password) {
        return retrofitInterface.userRegister(username, password);
    }

    /**
     * 登录
     **/
    public Observable<String> loginUser(String name, String password) {
        return retrofitInterface.loginUser(name, password);
    }

    /**
     * 提交需求
     **/
    public Observable<String> submitRequirements(String name, String phone, String demand, String type, String id) {
        return retrofitInterface.submitRequirements(name, phone, demand, type, id);
    }
}
