package com.yunyilian8.customserviceapp.service;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * retrofit网络请求接口
 * 包含
 * 1.请求的方式
 * 2.请求的路径
 * 3.请求的参数
 * 类说明
 */
public interface RetrofitInterface {


    /** 获取首页轮播图列表 */
    @POST("api/banner/index")
    Observable<String> callBannerList();

    /** 注册 **/
    @GET("api/register/register")
    Observable<String> userRegister(@Query("username") String username, @Query("password") String password);


    /** 登录 **/
    @POST("api/login/login")
    Observable<String> loginUser(@Query("username") String username, @Query("password") String password );

    /** 提交需求 */
    @POST("api/demand/demand")
    Observable<String> submitRequirements(@Query("name") String name, @Query("phone") String phone ,@Query("demand") String demand,@Query("type") String type,@Query("id") String id) ;

}
