package com.yunyilian8.customserviceapp.base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * @author wyb
 * @function listView对应adapter的抽取
 * @time 2016/7/8 14:10.
 */
public abstract class BaseListViewAdapter<T> extends BaseAdapter {

    public Context mContext;
    public List<T> mList;

    public BaseListViewAdapter(Context context, List<T> list) {
        this.mContext = context;
        this.mList = list;
    }

    public void setList(List<T> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return null == mList ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null == mList ? null : mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = baseGetView(position, convertView, parent);
        return view;
    }

    protected abstract View baseGetView(int position, View convertView, ViewGroup parent);
}
