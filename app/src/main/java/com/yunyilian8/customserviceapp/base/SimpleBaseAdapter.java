package com.yunyilian8.customserviceapp.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 类说明：
 * Created by Andy on 2016/4/21.
 */
public abstract class SimpleBaseAdapter<T> extends BaseAdapter {

    public Context mContext;
    protected List<T> mList;
    private LayoutInflater mInflater;

    public SimpleBaseAdapter(Context context) {
        init(context);
    }

    public SimpleBaseAdapter(Context context, List<T> list) {
        mList = list;
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    public abstract View getView(int position, View convertView,
                                 ViewGroup parent);

    public List<T> getList() {
        return mList;
    }

    public void setList(List<T> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    public void addList(List<T> nList) {
        if (null == mList) {
            mList = new ArrayList<T>();
        }
        mList.addAll(nList);
        notifyDataSetChanged();
    }

    protected LayoutInflater getInflater() {
        return mInflater;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList == null ? null : mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
