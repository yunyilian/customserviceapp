package com.yunyilian8.customserviceapp.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.view.LoadingView;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Andy on 16/4/9.
 */
public class BaseFragment extends Fragment {

    public static String TAG;//"TAG"为页面名称，可自定义
    protected LoadingView qjLoadingView;
    protected CompositeSubscription mCompositeSubscription;

    protected Context mCtx;
    protected Context mAppContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCtx = getActivity();
        mAppContext = mCtx.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeSubscription = new CompositeSubscription();
        qjLoadingView = new LoadingView(getActivity());
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();

    }

    protected void showLoadingView() {
        if (!qjLoadingView.isShowing())
            qjLoadingView.show();
    }

    protected void hideLoadingView() {
        if (qjLoadingView.isShowing()) {
            qjLoadingView.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCompositeSubscription.unsubscribe();
    }

    protected void loadDataFromNet(Subscriber subscriber, Observable observable) {
        mCompositeSubscription.add(
                observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber)
        );
    }

    protected void loadDataFromNet(Action1 action, Action1<Throwable> error, Observable observable) {
        mCompositeSubscription.add(
                observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(action, error)
        );

    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        //     overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        //    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        getActivity().overridePendingTransition(R.anim.push_right_in,R.anim.push_left_out);
    }
}
