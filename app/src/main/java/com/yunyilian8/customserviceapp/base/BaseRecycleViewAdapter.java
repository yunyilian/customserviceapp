package com.yunyilian8.customserviceapp.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * 作者：wyb on 2016/10/19 15:00
 * 邮箱：276698048@qq.com
 * 说明:RecycleViewAdapter 的基础类
 */

public abstract class BaseRecycleViewAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    public Object mList;
    public Context mContext;
    public LayoutInflater mInflater;

    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        return getHolder(parent, viewType);
    }

    public abstract T getHolder(ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(final T holder, int position) {
        if (mOnItemClickListener != null) {
            /**设置点击事件**/
            holder.itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int LayoutPosition = holder.getLayoutPosition();
                    mOnItemClickListener.OnItemClick(LayoutPosition, holder.itemView);
                }
            });

            /**设置长按事件**/
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int LayoutPosition = holder.getLayoutPosition();
                    mOnItemClickListener.OnItemLongClick(LayoutPosition, holder.itemView);
                    return false;
                }
            });
        }
        setViewHolder(holder, position);
    }

    public abstract void setViewHolder(T holder, int position);

    @Override
    public int getItemCount() {
        return ((List) mList).size();
    }

    /**
     * 声明一个接口，用于实现点击事件
     */
    public interface OnItemClickListener {
        void OnItemClick(int position, View view);

        void OnItemLongClick(int position, View view);
    }

    public OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}
