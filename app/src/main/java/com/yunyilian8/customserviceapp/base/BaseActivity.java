package com.yunyilian8.customserviceapp.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.view.LoadingView;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


/**
 * Created by Andy on 16/4/9.
 */
public class BaseActivity extends Activity {
    public Context mCtx;
    protected LoadingView qjLoadingView;
    protected CompositeSubscription mCompositeSubscription;
    public Context mAppContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mAppContext = getApplicationContext();
        mCompositeSubscription = new CompositeSubscription();
        mCtx = this;
        qjLoadingView = new LoadingView(mCtx);

    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeSubscription.unsubscribe();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
   //     overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
    }

    /**
     * 关闭当前界面,进入新的activity,解决退出和进入activity动画冲突的问题
     */
    public void finishToNewActivity() {
        super.finish();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
    //    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        overridePendingTransition(R.anim.push_right_in,R.anim.push_left_out);

    }

    protected void showLoadingView() {
        if (!qjLoadingView.isShowing() && !isFinishing())
            qjLoadingView.show();
    }

    protected void hideLoadingView() {
        if (!isFinishing() && qjLoadingView.isShowing()) {
            qjLoadingView.dismiss();
        }
    }

    protected void loadDataFromNetAndTimeout(Subscriber subscriber, Observable observable, long timeout) {
        mCompositeSubscription.add(
                observable.timeout(timeout, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber)
        );
    }

    protected void loadDataFromNet(Subscriber subscriber, Observable observable) {
        mCompositeSubscription.add(
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber)
        );
    }

    protected void loadDataFromNet(Action1 action, Action1<Throwable> error, Observable observable) {
        mCompositeSubscription.add(
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(action,error)
        );
    }

}
