package com.yunyilian8.customserviceapp.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.yunyilian8.customserviceapp.R;
import com.yunyilian8.customserviceapp.utils.ApkInfoUtil;
import com.yunyilian8.customserviceapp.view.TitleBarLayout;

public abstract class BaseWebViewActivity extends BaseActivity implements TitleBarLayout.TitleBarListener{

    protected TitleBarLayout titleBarLayout;
    protected WebView mWebView;
    public WebSettings mWebSettings;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_web_view);

        initTitle();
        initView();

        initData();
    }


    protected void initView() {

        mWebView = (WebView) findViewById(R.id.activity_web_view);

        mWebSettings = mWebView.getSettings();
        mWebSettings.setDefaultTextEncodingName("utf-8");
        mWebSettings.setSupportZoom(false);
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
        setTextSize(mWebSettings);
        mWebSettings.setLoadWithOverviewMode(true);
        mWebSettings.setDatabaseEnabled(true);// 启用数据库
        String databaseDir = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();// 设置定位的数据库路径
        mWebSettings.setGeolocationDatabasePath(databaseDir);
        mWebSettings.setGeolocationEnabled(true);// 启用地理定位
        mWebSettings.setDomStorageEnabled(true);// 最重要的方法，一定要设置，这就是出不来的主要原因
        mWebView.setFocusableInTouchMode(true);
        mWebView.requestFocus();
        mWebView.requestFocusFromTouch();
        mWebView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);



    }

    @TargetApi(19)
    private void setTextSize(WebSettings mWebSettings){
        if (getURL().startsWith("file") && ApkInfoUtil.getAndroidSDKVersion() >= 19){
            mWebSettings.setLayoutAlgorithm(LayoutAlgorithm.TEXT_AUTOSIZING);
        }else{
            mWebSettings.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
        }
    }


    protected void initTitle() {
        titleBarLayout = (TitleBarLayout) findViewById(R.id.titlebar_layout);
        titleBarLayout.setTitleBarListener(this);
    }

    abstract protected String getURL();


    protected void initData() {

        mWebView.loadUrl(getURL());
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (showLoading())
                    showLoadingView();
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);

            }
            @Override
            public void onPageFinished(WebView view, String url) {

                super.onPageFinished(view, url);
                if (showLoading())
                    hideLoadingView();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                if (!TextUtils.isEmpty(title)) {
                    titleBarLayout.setTitleText(title);
                }
                super.onReceivedTitle(view, title);
            }
        });
    }

    protected abstract boolean showLoading();

    @Override
    public void onBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()){
            mWebView.goBack();
        }else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public void onActionImageClick() {

    }

    @Override
    public void onActionClick() {

    }

    @Override
    /**
     * 设置回退
     * 覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            /**goBack()表示返回WebView的上一页面**/
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
