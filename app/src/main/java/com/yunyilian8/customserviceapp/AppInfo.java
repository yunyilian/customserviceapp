package com.yunyilian8.customserviceapp;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;

import com.yunyilian8.customserviceapp.bean.UserInfoBean;
import com.yunyilian8.customserviceapp.utils.Contants;

import org.litepal.LitePalApplication;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.Subject;

public class AppInfo extends LitePalApplication {
    public static Context mMainActivityContext;
    public static int mScreenHeight = 0;
    public static int mScreenWidth = 0;
    public static String mTokenId = "";
    public static String mTel = "";
    public static List<Subject> mAppSubList;
    public static int mCityId = 0 ;
    public static UserInfoBean mInfo;


    public static boolean mFirstReceive = true;
    private static ArrayList<Activity> list = new ArrayList<Activity>();
    private static final String LOG_DIR = Contants.ExceptionLogPath;
    private static final String LOG_NAME =  "superBrain_log.txt";

 //   private RefWatcher refWatcher;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //内存泄漏检测工具
      //refWatcher = LeakCanary.install(this);
        initExceptionMethod();
        initScreenInfo();
    }

    private void initExceptionMethod() {
        /**系统发生错误回调**/
    }

    private void initScreenInfo() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        mScreenWidth = displayMetrics.widthPixels;
        mScreenHeight = displayMetrics.heightPixels;
    }



    public static void clearData() {
        mTokenId = "";
        mTel = "";
        mAppSubList = null;
        mCityId = 0;
        mInfo = null;
    }


    /**
     * 关闭Activity列表中的所有Activity
     */
    public void exit() {
        for (Activity activity : list) {
            if (null != activity) {
                activity.finish();
            }
        }
        // 杀死该应用进程
        android.os.Process.killProcess(android.os.Process.myPid());
    }
    /**
     * Activity关闭时，删除Activity列表中的Activity对象
     */
    public void removeActivity(Activity a) {
        list.remove(a);
    }

    /**
     * 向Activity列表中添加Activity对象
     */
    public static void addActivity(Activity a) {
        list.add(a);
        /**设置activity的状态栏字体颜色为黑色**/
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            /**api5.0及以上**/
//            StatusBarUtil.StatusBarLightMode(a);
//            StatusBarUtil.setStatusBarColor(a,R.color.white);
        }else {
            /**不做改变,默认系统主题**/
        }
    }
}
